import pandas as pd
from glob import glob

# Utilities to read EQTransformer output csv file

# Function written by Jonathan Smith, Caltech
# Shared with me by Jack Wilding, Caltech
def EQTransform2DataFrame(inpath_folder):
   '''
        Function to convert the EQTransform phase pick files (csv) to a pandas DataFrame format
        Inputs:
            inpath_folder - The host directory of the EQTransformer detection
        Outputs:
            pcks_gpd - EQTransformer picks as a pandas DataFrame
   '''
   pcks_gpd = pd.DataFrame(columns=['Network','Station','Phase','PickTime','Prob','ProbErr'])
   print(inpath_folder)
   for indx, fld in enumerate(glob('{}/*/*'.format(inpath_folder))): # sub-folders separated by date, then sub-folders separated by station, then csv
#   for indx, fld in enumerate(glob('{}/*'.format(inpath_folder))): # sub-folders separated by station, then csv
      print(indx, fld)
      pcks_eqt = pd.read_csv('{}/X_prediction_results.csv'.format(fld))
      # Select only p-wave picks and append to blank dataframe
      tmp_ppicks = pcks_eqt[['network','station','p_arrival_time','p_probability']].dropna().reset_index(drop=True)
      tmp_spicks = pcks_eqt[['network','station','s_arrival_time','s_probability']].dropna().reset_index(drop=True)
      if len(tmp_ppicks) > 0:
         tmp_ppicks['p_arrival_time'] = pd.to_datetime(tmp_ppicks['p_arrival_time']).dt.strftime('%Y-%m-%dT%H:%M:%S.%f')
         tmp_ppicks['Phase']     = 'P'
         tmp_ppicks['ProbErr']    = 0.01
         tmp_ppicks = tmp_ppicks[['network','station','Phase','p_arrival_time','p_probability','ProbErr']]
         tmp_ppicks.columns = ['Network','Station','Phase','PickTime','Prob','ProbErr']
         tmp_ppicks['Network'] = tmp_ppicks['Network'].str.strip()
         tmp_ppicks['Station'] = tmp_ppicks['Station'].str.strip()
         pcks_gpd = pcks_gpd.append(tmp_ppicks)
      if len(tmp_spicks) > 0:
         tmp_spicks['s_arrival_time'] = pd.to_datetime(tmp_spicks['s_arrival_time']).dt.strftime('%Y-%m-%dT%H:%M:%S.%f')
         tmp_spicks['Phase']     = 'S'
         tmp_spicks['ProbErr']    = 0.01
         tmp_spicks = tmp_spicks[['network','station','Phase','s_arrival_time','s_probability','ProbErr']]
         tmp_spicks.columns = ['Network','Station','Phase','PickTime','Prob','ProbErr']
         tmp_spicks['Network'] = tmp_spicks['Network'].str.strip()
         tmp_spicks['Station'] = tmp_spicks['Station'].str.strip()
         pcks_gpd = pcks_gpd.append(tmp_spicks)
   pcks_gpd_sorted = pcks_gpd.sort_values(by=['PickTime'], ascending=True)
   return pcks_gpd_sorted
#   return pcks_gpd
