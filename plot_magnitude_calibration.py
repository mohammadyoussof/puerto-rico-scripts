import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams

#rcParams.update({'font.size': 24})
rcParams.update({'font.size': 30}) #poster
#rcParams['pdf.fonttype'] = 42
rcParams['font.family'] = "sans-serif"
rcParams['font.sans-serif'] = "Helvetica"



#in_hinv_sum_file = '../FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/events_MATCH_magcat_eqt_2018_2020.txt'
#out_mag_plot_dir = '../Plots/'
#out_calib_plot_file = 'FullEQTransformer_Magnitude_Calibration.pdf'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_magcat_EQT_20180101_20211001.txt'
#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'final_magnitude_calibration_us_EQT_20180101_20211001.pdf'
#out_resid_plot_file = 'final_magnitude_residual_us_EQT_20180101_20211001.pdf'

#in_hinv_sum_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_magcat_EQT_20180101_20220101.txt'
#out_mag_plot_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/CompPlots/'
#out_calib_plot_file = 'final_magnitude_calibration_us_EQT_20180101_20220101.pdf'
#out_resid_plot_file = 'final_magnitude_residual_us_EQT_20180101_20220101.pdf'

in_hinv_sum_file = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MATCH_magcat_EQT_20180101_20220101.txt'
out_mag_plot_dir = '../EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/CompPlots/'
out_calib_plot_file = 'final_magnitude_calibration_EQT_20180101_20220101.pdf'
out_resid_plot_file = 'final_magnitude_residual_EQT_20180101_20220101.pdf'


all_catalog_mag = []
all_catalog_ML = []
with open(in_hinv_sum_file, 'r') as fin:
   for line in fin:
      ml_mag = 0.01*float(line[147:150])
      cat_line = line[179:]
      split_line = cat_line.split()
      cat_mag = float(split_line[4])
      all_catalog_mag.append(cat_mag)
      all_catalog_ML.append(ml_mag)
#      print(ml_mag, cat_mag)
print(len(all_catalog_mag), len(all_catalog_ML))


# After magnitude calibration, show that magnitudes match: plot catalog magnitude vs calculated ML
equal_line = np.linspace(-1, 7, 100)
plt.figure(figsize=(6,6))
plt.scatter(all_catalog_mag, all_catalog_ML, facecolors='none', edgecolors='k', linewidths=0.1)
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlim([0, 7])
plt.ylim([0, 7])
plt.xticks([0, 1, 2, 3, 4, 5, 6, 7])
plt.yticks([0, 1, 2, 3, 4, 5, 6, 7])
plt.xlabel('Catalog magnitude $M_{cat}$')
plt.ylabel('Local magnitude $M_{L}$')
plt.tight_layout()
plt.savefig(out_mag_plot_dir+out_calib_plot_file)

# Plot residuals
resid_mag = np.subtract(all_catalog_ML, all_catalog_mag)
print("Min residual: ", min(resid_mag))
print("Max residual: ", max(resid_mag))
zero_line = np.zeros(100)
plt.figure(figsize=(6,6))
plt.scatter(all_catalog_mag, resid_mag, facecolors='none', edgecolors='k', linewidths=0.1)
plt.plot(equal_line, zero_line, '--', color='k')
plt.xlim([0, 7])
#plt.ylim([-1, 1])
plt.ylim([-4, 4])
plt.xticks([0, 1, 2, 3, 4, 5, 6, 7])
#plt.yticks([-1, -0.5, 0, 0.5, 1])
plt.xlabel('Catalog magnitude $M_{cat}$')
plt.ylabel('Residual: $M_{L}$ - $M_{cat}$')
plt.tight_layout()
plt.savefig(out_mag_plot_dir+out_resid_plot_file)

