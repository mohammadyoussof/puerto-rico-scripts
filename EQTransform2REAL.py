import pandas as pd
import datetime
import json
import os
import time
import utils_eqt

# EQTransform2REAL.py: Script to convert picks from EQTransformer csv format to REAL input format

#--------------------------START OF INPUTS------------------------
#in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
#in_pick_dir = '../LargeAreaEQTransformer/mseed_detections/'
#out_pick_dir = '../LargeAreaEQTransformer/REAL/Pick/'
#tstart="2020-01-07 00:00:00.00"
#tend="2020-01-14 00:00:00.00"

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list.json'
#out_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json' # only stations with picks
#in_pick_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/mseed_detections/'
#out_pick_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/Pick/'
#tstart="2020-01-07 00:00:00.00"
#tend="2020-01-14 00:00:00.00"

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list.json'
#out_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json' # only stations with picks
#in_pick_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/mseed_detections/'
#out_pick_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/Pick/'
#tstart="2019-12-28 00:00:00.00"
#tend="2020-01-14 00:00:00.00"

in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list.json'
out_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json' # only stations with picks
in_pick_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/mseed_detections/'
out_pick_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/Pick/'
tstart="2018-01-01 00:00:00.00"
#tend="2021-06-01 00:00:00.00"
#tend="2021-08-01 00:00:00.00"
#tend="2021-09-01 00:00:00.00"
#tend="2021-10-01 00:00:00.00"
#tend="2021-12-01 00:00:00.00"
tend="2022-01-01 00:00:00.00"

#--------------------------END OF INPUTS------------------------

pcks_gpd = utils_eqt.EQTransform2DataFrame(in_pick_dir)

if os.path.exists(out_pick_dir):
   os.system("rm -r "+out_pick_dir)
   os.makedirs(out_pick_dir)

start_date = datetime.datetime.strptime(tstart, "%Y-%m-%d %H:%M:%S.%f")
end_date = datetime.datetime.strptime(tend, "%Y-%m-%d %H:%M:%S.%f")

# Output REAL pick time file for each day
delta = datetime.timedelta(days=1)
stations_ = json.load(open(in_station_file))
sta_used = set()

# Loop over days
t0 = time.time()
while (start_date < end_date):
   # Create directory for start_date day if it doesn't already exist
   out_pick_date_dir = out_pick_dir+datetime.datetime.strftime(start_date, "%Y%m%d")
   if not os.path.exists(out_pick_date_dir):
      os.makedirs(out_pick_date_dir)
   next_date = start_date + delta
   print("current start_date = ", datetime.datetime.strftime(start_date, "%Y%m%d"), ", next_date = ", next_date)

   # Get all picks between start_date and next_date, regardless of station
   picks_time = pcks_gpd.loc[(pd.to_datetime(pcks_gpd['PickTime']) >= start_date) & (pd.to_datetime(pcks_gpd['PickTime']) < next_date)]

   # Loop over stations
   count_sta = 0
   for sta,val in stations_.items():
      count_sta += 1
      # Get picks for this station between start_date and next_date
      picks_time_sta = picks_time.loc[(picks_time['Network'] == val['network']) & (picks_time['Station'] == sta)]

      # Get P picks for this station between start_date and next_date, output in REAL format
      picks_time_sta_P = picks_time_sta.loc[(picks_time_sta['Phase'] == 'P')]
      out_pickP_file = out_pick_date_dir+'/'+val['network']+'.'+sta+'.P.txt'
      picks_outP = picks_time_sta_P.filter(['PickTime', 'Prob', 'ProbErr'])
      picks_outP.PickTime = (pd.to_datetime(picks_outP['PickTime']) - start_date).dt.total_seconds().round(4)
      picks_outP.ProbErr = 0.0
      if (len(picks_outP) > 0): # avoid outputting files with no picks
         picks_outP.to_csv(out_pickP_file,header=False,index=False,sep=' ')
         sta_used.add(sta)
         print(out_pickP_file)

      # Get S picks for this station between start_date and next_date, output in REAL format
      picks_time_sta_S = picks_time_sta.loc[(picks_time_sta['Phase'] == 'S')]
      out_pickS_file = out_pick_date_dir+'/'+val['network']+'.'+sta+'.S.txt'
      picks_outS = picks_time_sta_S.filter(['PickTime', 'Prob', 'ProbErr'])
      picks_outS.PickTime = (pd.to_datetime(picks_outS['PickTime']) - start_date).dt.total_seconds().round(4)
      picks_outS.ProbErr = 0.0
      if (len(picks_outS) > 0): # avoid outputting files with no picks
         picks_outS.to_csv(out_pickS_file,header=False,index=False,sep=' ')
         sta_used.add(sta)
         print(out_pickS_file)

   start_date = next_date

tfinal = time.time() - t0
print("Runtime for EQTransform2REAL convert_picks: ", tfinal)

# Output only stations with picks to json file
sta_out_used = dict()
for sta in sta_used:
   sta_out_used[sta] = stations_[sta]
json.dump(sta_out_used, open(out_station_file, 'w'), sort_keys=True)
print("Number of total stations in station_list.json = ", count_sta)
print("Number of stations with picks in station_list_edited.json = ", len(sta_out_used))

