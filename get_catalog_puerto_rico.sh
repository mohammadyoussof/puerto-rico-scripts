#!/bin/bash

# Get catalog with USGS ComCat API
# https://earthquake.usgs.gov/fdsnws/event/1/
#
# 2020-11-02	C. Yoon    First created

# Catalog input/output - times
# Keep track of date when catalog was downloaded, since catalog changes over time (from analyst progress)

## EQT_20200107_20200114
#t_start='2020-01-07T00:00:00'
#t_end='2020-01-14T00:00:00'
#out_file='../Catalog/EQT_20200107_20200114/catalog_puerto_rico_20200107_20200114_download20210601.txt'
##out_file='../Catalog/EQT_20200107_20200114/catalog_puerto_rico_20200107_20200114_catalogus_contributorus_download20210601.txt'
##out_file='../Catalog/catalog_puerto_rico_20200107_20200114.txt'

## EQT_20191228_20200114
#t_start='2019-12-28T00:00:00'
#t_end='2020-01-14T00:00:00'
#out_file='../Catalog/EQT_20191228_20200114/catalog_puerto_rico_20191228_20200114_download20210608.txt'
##out_file='../Catalog/EQT_20191228_20200114/catalog_puerto_rico_20191228_20200114_catalogus_contributorus_download20210608.txt'

# Note: us catalog retrieval needs to be done 6 months at a time; upper limit for regular catalog retrieval is 20000 events
# EQT_20180101_20210601
#t_start='2018-01-01T00:00:00'
#t_end='2018-07-01T00:00:00'
#t_start='2018-07-01T00:00:00'
#t_end='2019-01-01T00:00:00'
#t_start='2019-01-01T00:00:00'
#t_end='2019-07-01T00:00:00'
#t_start='2019-07-01T00:00:00'
#t_end='2020-01-01T00:00:00'
#t_start='2020-01-01T00:00:00'
#t_end='2020-07-01T00:00:00'
#t_start='2020-07-01T00:00:00'
#t_end='2021-01-01T00:00:00'
#t_start='2021-01-01T00:00:00'
#t_end='2021-06-01T00:00:00'
#out_file='../Catalog/EQT_20180101_20210601/catalog_puerto_rico_20180101_20210601_download20210610.txt'
##out_file='../Catalog/EQT_20180101_20210601/catalog_puerto_rico_20180101_20210601_catalogus_contributorus_download20210610.txt'

#t_start='2018-01-01T00:00:00'
##t_end='2021-01-01T00:00:00'
##t_start='2021-01-01T00:00:00'
#t_end='2021-10-01T00:00:00'
#out_file='../Catalog/EQT_20180101_20211001/catalog_puerto_rico_20180101_20211001_download20211020.txt'
##out_file='../Catalog/EQT_20180101_20211001/catalog_puerto_rico_20180101_20211001_catalogus_contributorus_download20211020.txt'

t_start='2018-01-01T00:00:00'
#t_end='2021-01-01T00:00:00'
#t_start='2021-01-01T00:00:00'
t_end='2022-01-01T00:00:00'
out_file='../Catalog/EQT_20180101_20220101/catalog_puerto_rico_20180101_20220101_download20220111.txt'
#out_file='../Catalog/EQT_20180101_20220101/catalog_puerto_rico_20180101_20220101_catalogus_contributorus_download20220111.txt'

#t_start='2018-01-01T00:00:00'
#t_end='2020-11-01T00:00:00'
#out_file='../Catalog/catalog_puerto_rico_2018_2020.txt'

#t_start='2018-01-01T00:00:00'
##t_end='2021-01-04T00:00:00'
#out_file='../Catalog/catalog_box_puerto_rico_20180101_20210104.txt'

#t_start='2018-01-01T00:00:00'
#t_end='2021-03-16T00:00:00'
#out_file='../Catalog/catalog_box_puerto_rico_20180101_20210316.txt'

#t_start='2018-01-01T00:00:00'
#t_end='2021-02-01T00:00:00'
#out_file='../Catalog/catalog_puerto_rico_20180101_20210201.txt'

# Catalog lat/lon boundaries
min_lat=17
max_lat=19
min_lon=-68
max_lon=-65
out_format='text'

## box
#t_start='2018-01-01T00:00:00'
##t_end='2021-01-04T00:00:00'
#t_end='2021-03-16T00:00:00'
#min_lat=17.6
#max_lat=18.3
#min_lon=-67.3
#max_lon=-66.4
#---------------------------------------

data_src='https://earthquake.usgs.gov/fdsnws/event/1/query'
echo ${data_src}

## Minimum magnitude query
#min_mag=5
#out_file='../Catalog/test_catalog_large.txt'
#query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&orderby='time-asc'\&minmagnitude=${min_mag}

# No minimum magnitude query (second line: specific catalog and contributor)
query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&orderby='time-asc'
#query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&catalog=us\&contributor=us\&orderby='time-asc'

# Retrieve catalog from ComCat
echo ${query_str}
wget ${query_str} -O ${out_file}



