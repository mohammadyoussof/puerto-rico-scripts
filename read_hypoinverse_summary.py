from obspy import UTCDateTime
import utils_hypoinverse as utils_hyp

# Read HYPOINVERSE summary output file
# Output file for plotting on GMT map

#in_hinv_sum_file = '../TestEQTransformer/association/locate_pr.sum'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/TestEQTransformer_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt'
#in_hinv_sum_file = '../LargeAreaEQTransformer/association/locate_pr.sum'

#in_hinv_sum_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2020-01-07T00:00:00')
#output_plot_file = '../Catalog/LargeAreaEQTransformerModel2_REAL_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt'
##output_plot_file = '../Catalog/LargeAreaEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt'

#in_hinv_sum_file = '../LargeAreaEQTransformer/association/merged_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2020-01-07T00:00:00')
#output_plot_file = '../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt'

#in_hinv_sum_file = '../LargeAreaEQTransformer/association/events_MATCH_magcat_eqt_20200107_20200114.txt'
#catalog_start_time = UTCDateTime('2020-01-07T00:00:00')
#output_plot_file = '../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_MATCH_20200107_20200114.txt'

#in_hinv_sum_file = '../LargeAreaEQTransformer/REAL/Events/events_MATCH_magcat_eqt_20200107_20200114.txt'
#catalog_start_time = UTCDateTime('2020-01-07T00:00:00')
#output_plot_file = '../Catalog/LargeAreaEQTransformerModel2_REAL_HYPOINVERSE_puerto_rico_MATCH_20200107_20200114.txt'
##output_plot_file = '../Catalog/LargeAreaEQTransformer_REAL_HYPOINVERSE_puerto_rico_MATCH_20200107_20200114.txt'

#in_hinv_sum_file = '../LargeAreaEQTransformer/association/events_NEW_magcat_eqt_20200107_20200114.txt'
#catalog_start_time = UTCDateTime('2020-01-07T00:00:00')
#output_plot_file = '../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_NEW_20200107_20200114.txt'

#in_hinv_sum_file = '../LargeAreaEQTransformer/REAL/Events/events_NEW_magcat_eqt_20200107_20200114.txt'
#catalog_start_time = UTCDateTime('2020-01-07T00:00:00')
#output_plot_file = '../Catalog/LargeAreaEQTransformerModel2_REAL_HYPOINVERSE_puerto_rico_NEW_20200107_20200114.txt'
##output_plot_file = '../Catalog/LargeAreaEQTransformer_REAL_HYPOINVERSE_puerto_rico_NEW_20200107_20200114.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt'

#in_hinv_sum_file = '../FullEQTransformer/REAL/HYPOINVERSE/merged_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt'

#in_hinv_sum_file = '../FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/combined_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/events_MATCH_magcat_eqt_2018_2020.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_MATCH_2018_2020.txt'

#in_hinv_sum_file = '../FullEQTransformer/REAL/HYPOINVERSE/events_MATCH_magcat_eqt_2018_2020.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_MATCH_2018_2020.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/events_NEW_magcat_eqt_2018_2020.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_NEW_2018_2020.txt'

#in_hinv_sum_file = '../FullEQTransformer/REAL/HYPOINVERSE/events_NEW_magcat_eqt_2018_2020.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_NEW_2018_2020.txt'

#-----------------------------------
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/combined_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2020-01-07T00:00:00')
#output_plot_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/combined_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2020-01-07T00:00:00')
#output_plot_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/ORIGINAL_merged_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2020-01-07T00:00:00')
#output_plot_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_ORIGINAL_merged_real_magcat_locate_pr.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2020-01-07T00:00:00')
#output_plot_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_merged_real_magcat_locate_pr.txt'

#-----------------------------------
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/events_MATCH_magcat_EQT_20180101_20210601.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOINVERSE_VELPRSN_events_MATCH_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/events_NEW_magcat_EQT_20180101_20210601.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOINVERSE_VELPRSN_events_NEW_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_MATCH_magcat_EQT_20180101_20210601.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_events_MATCH_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_NEW_magcat_EQT_20180101_20210601.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_events_NEW_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/combined_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_MATCH_magcat_EQT_20180101_20211001.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_events_MATCH_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_NEW_magcat_EQT_20180101_20211001.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_events_NEW_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/combined_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'
#
####
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_events_MISSED_magcalcml.txt'

#-----------------------------------
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MATCH_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_events_MATCH_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_NEW_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_events_NEW_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_events_MISSED_magcalcml.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/combined_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MATCH_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_events_MATCH_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_NEW_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_events_NEW_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_events_MISSED_magcalcml.txt'

in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/combined_real_magcat_locate_pr.sum'
catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'


fout = open(output_plot_file, 'w')
with open(in_hinv_sum_file, 'r') as fin:
   for line in fin:
      origin_time = utils_hyp.get_origin_time_hypoinverse_file(line)
      num_sec = origin_time - catalog_start_time

      [lat_deg, lon_deg, depth_km] = utils_hyp.get_lat_lon_depth_hypoinverse_file(line)
      evid = utils_hyp.get_event_id_hypoinverse_file(line)
      mag = 0.01*float(line[147:150])

      fout.write(("%f %f %f %f %f %s\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
      print(line)
fout.close()

