# dtcc file input: dtcc file
# dtcc file output: dtcc file without bad differential times

#in_dtcc_file = '../LargeAreaEQTransformer/HYPODD/combined_LargeAreaEQTransformer_puerto_rico_dt_cc.txt'
#out_dtcc_file = '../LargeAreaEQTransformer/HYPODD/clean_combined_LargeAreaEQTransformer_puerto_rico_dt_cc.txt'
in_dtcc_file = '../LargeAreaEQTransformer/HYPODD/out_LargeAreaEQTransformer_puerto_rico_dt_cc.txt'
out_dtcc_file = '../LargeAreaEQTransformer/HYPODD/clean_out_LargeAreaEQTransformer_puerto_rico_dt_cc.txt'

max_dt = 5.0

fout = open(out_dtcc_file,'w')
with open(in_dtcc_file,'r') as fin:
   dtcc_list = []
   ev_line = ''
   for line in fin:
      if (line[0] == '#'):
         # From the previous event pair
         if (len(dtcc_list) > 0):
            fout.write(ev_line)
            for dtpair in dtcc_list:
               fout.write(dtpair)
         ev_line = line
         dtcc_list.clear()
      else:
         split_line = line.split()
         val_dt = float(split_line[1])
         if (abs(val_dt) <= max_dt):
            dtcc_list.append(line)

# Last remaining event
if (len(dtcc_list) > 0):
   fout.write(ev_line)
   for dtpair in dtcc_list:
      fout.write(dtpair)

fout.close()
