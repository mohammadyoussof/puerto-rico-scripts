import datetime
import numpy as np

# Script to convert HypoSVI CSV catalog for GMT plot

#in_hsvi_sum_file = '../Catalog/Events_FullEQTransformer_HypoSVI_Catalog.csv'
in_hsvi_sum_file = '../Catalog/EventsALL_FullEQTransformer_HypoSVI_Catalog.csv'
in_hinv_sum_file = '../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020.txt' # get magnitudes
catalog_start_time = '2018-01-01 00:00:00'
#out_hsvi_file = '../Catalog/Events_FullEQTransformer_HypoSVI_Catalog_2018_2020.txt'
#out_hsvi_file = '../Catalog/Events_FullEQTransformer_HypoSVI_Catalog_2018_2020_mag.txt'
out_hsvi_file = '../Catalog/EventsALL_FullEQTransformer_HypoSVI_Catalog_2018_2020_mag.txt'

[hinv_ot, hinv_lat, hinv_lon, hinv_depth, hinv_mag, hinv_evid] = np.loadtxt(in_hinv_sum_file, unpack=True)

catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%d %H:%M:%S')
diff_thresh = 40.0 # difference between HypoSVI and Hypoinverse origin times (seconds)
#mag = 2.0 # TODO placeholder for magnitude
diff_min_sec_arr = []
nread = 0
nev_hsvi = 0
fout = open(out_hsvi_file, 'w')
with open(in_hsvi_sum_file, 'r') as fin:
   for line in fin:
      if (line[0] == 'E'): # skip first line
         continue
      split_line = line.strip('\n').split(',')
      origin_time = datetime.datetime.strptime(split_line[1][:-3], '%Y-%m-%d %H:%M:%S.%f')
      num_sec = (origin_time - catalog_ref_time).total_seconds()
      lat_deg = split_line[3]
      lon_deg = split_line[2]
      depth_km = split_line[4]
      evid = split_line[0]

      # Get magnitude from hypoinverse file, based on matching origin times
      diff_sec = abs(num_sec - hinv_ot)
      diff_min_sec = min(diff_sec)
      diff_min_sec_arr.append(diff_min_sec)
      if (diff_min_sec < diff_thresh):
         ind_min_sec = np.argmin(diff_sec)
         mag = hinv_mag[ind_min_sec]
         fout.write(("%f %s %s %s %f %s %s %s %s\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid, split_line[5], split_line[6], split_line[7]))
         nev_hsvi += 1
#         print(diff_min_sec, evid, num_sec, int(hinv_evid[ind_min_sec]), hinv_ot[ind_min_sec], mag)
      else:
         print("Warning: diff_min_sec above threshold", diff_min_sec, evid, num_sec, int(hinv_evid[ind_min_sec]), hinv_ot[ind_min_sec], mag)

#      fout.write(("%f %s %s %s %f %s %s %s %s\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid, split_line[5], split_line[6], split_line[7]))
      nread += 1
#      print(split_line, origin_time)
fout.close()
print("nread = ", nread)
print("nev_hsvi = ", nev_hsvi)
