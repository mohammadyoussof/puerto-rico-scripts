import glob
import os
import numpy as np
from obspy import read
from obspy.core.stream import Stream

# Output continuous mseed files with zeropad at the end
# https://github.com/smousavi05/EQTransformer/issues/56

#base_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/'
#in_mseed_str = 'downloads_mseeds'
#out_mseed_str = 'zeropad_mseeds'

#base_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/'
#in_mseed_str = 'downloads_mseeds'
#out_mseed_str = 'zeropad_mseeds'

base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/'
in_mseed_str = 'downloads_mseeds'
out_mseed_str = 'zeropad_mseeds'
#in_mseed_str = 'downloads_mseeds/2021_12'
#out_mseed_str = 'zeropad_mseeds/2021_12'

zeropad_seconds = 7200 # 2 hours

in_mseed_dir = base_dir+in_mseed_str+'/'
out_mseed_dir = base_dir+out_mseed_str+'/'
if not os.path.exists(out_mseed_dir):
   os.makedirs(out_mseed_dir)

mseed_files = glob.glob(in_mseed_dir+'*/*/*mseed', recursive=True)
#mseed_files = glob.glob(in_mseed_dir+'*/*mseed', recursive=True) # use when adding subdirectory: downloads_mseeds/2021_12
print(len(mseed_files))

no_zeropad_files = []
for infile in mseed_files:
   st_out = Stream()
   st_in = read(infile)
   st_in.merge(fill_value=0) # merge continuous data first
   for tr in st_in:
      print(tr)
      nsamp = int(zeropad_seconds * tr.stats.sampling_rate) # number of samples - extra zeros CY
      tr_empty = tr.slice(tr.stats.starttime, tr.stats.starttime + zeropad_seconds) # add an extra time period of zeros (at end) to avoid lack of picks in last hour CY
      tr_empty.data = np.zeros(nsamp, dtype=np.int32) # extra time period of zeros CY
      tr_empty.stats.starttime = tr.stats.endtime # set correct start time for extra time period of zeros (at end), assuming daylong miniseed files CY
      tr_out = tr.__add__(tr_empty, method=1) # Add zeros to the end CY
      print(tr_out)
      st_out += tr_out

   out_mseed_file = infile.replace(in_mseed_str, out_mseed_str)
   print(infile, out_mseed_file)
   [out_dir, out_file] = os.path.split(out_mseed_file)
   if not os.path.exists(out_dir):
      os.makedirs(out_dir)

   try:
      st_out.write(out_mseed_file)
   except Exception:
      print("WARNING: UNABLE TO OUTPUT ZERO PAD FILE: ", out_mseed_file, " WILL COPY ORIGINAL FILE: ", infile)
      os.system("cp "+infile+" "+out_dir)
      no_zeropad_files.append(infile)

print("Number of files unable to zeropad: ", len(no_zeropad_files))
print(no_zeropad_files)

