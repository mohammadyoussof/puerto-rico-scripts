from EQTransformer.utils.downloader import makeStationList

# eqt_get_station_list.py: script to get list of stations to download data


#--------------------------START OF INPUTS------------------------
### Puerto Rico data
clientlist=["IRIS"]
minlat=17
maxlat=19
minlon=-68
maxlon=-65

# Download these channels, not ALL channels
chan_priority_list=["HH[ZNE12]", "BH[ZNE12]", "EH[ZNE12]", "HN[ZNE12]"]

### Time duration should be for entire time period of interest

## EQT_20200107_20200114
#tstart="2020-01-07 00:00:00.00"
#tend="2020-01-14 00:00:00.00"
#out_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list.json'

## EQT_20191228_20200114
#tstart="2019-12-28 00:00:00.00"
#tend="2020-01-14 00:00:00.00"
#out_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list.json'

# EQT_20180101_20220101
tstart="2018-01-01 00:00:00.00"
#tend="2021-06-01 00:00:00.00"
#tend="2021-08-01 00:00:00.00" #UPDATE
#tend="2021-09-01 00:00:00.00" #UPDATE
#tend="2021-10-01 00:00:00.00" #UPDATE
#tend="2021-12-01 00:00:00.00" #UPDATE
tend="2022-01-01 00:00:00.00" #UPDATE
out_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list.json'

#--------------------------END OF INPUTS------------------------

# Get list of stations
makeStationList(json_path=out_station_json_file, client_list=clientlist,
   min_lat=minlat, max_lat=maxlat, min_lon=minlon, max_lon=maxlon,
   start_time=tstart, end_time=tend, 
   channel_list=chan_priority_list,
#   channel_list=[], # don't use this, since it gets ALL channels
   filter_network=["SY"], filter_station=[])
