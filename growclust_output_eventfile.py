
in_phase_file = '../LargeAreaEQTransformer/HYPODD/pr_phases_hypoDD.txt'
out_event_file = '../LargeAreaEQTransformer/GrowClust/IN/pr_evlist.txt'

fout = open(out_event_file, 'w')
with open(in_phase_file, 'r') as fin:
   for line in fin:
      if (line[0] == '#'):
         fout.write(line[2:])
fout.close()
