import json
import time
import numpy as np
import utils_hypoinverse as utils_hyp
import utils_magnitude as utils_mag

# Calibrate local magnitude (Wood-Anderson) for EQTransformer events with magnitudes from matching catalog events


#in_hinv_arc_file = '../LargeAreaEQTransformer/association/merged_locate_pr.arc' # use the merged file
#in_match_catalog_file = '../LargeAreaEQTransformer/association/events_MATCH_eqt_20200107_20200114.txt'
##in_match_catalog_file = '../LargeAreaEQTransformer/association/TEST_events_MATCH_eqt_20200107_20200114.txt'
#in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
##in_event_file_dir = '../LargeAreaEQTransformer/EventFiles/'
#in_event_file_dir = '../LargeAreaEQTransformer/EventFiles120/'
#in_inv_dir = '../LargeAreaEQTransformer/downloads_mseeds/StationXML/'
##out_calib_file = '../LargeAreaEQTransformer/association/magnitude_calibration_20200107_20200114.txt'
##out_calib_file = '../LargeAreaEQTransformer/association/constrainwindow_magnitude_calibration_20200107_20200114.txt'
#out_calib_file = '../LargeAreaEQTransformer/association/constrainwindow120_magnitude_calibration_20200107_20200114.txt'

#in_hinv_arc_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '../LargeAreaEQTransformer/REAL/Events/events_MATCH_eqt_20200107_20200114.txt'
#in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
##in_event_file_dir = '../LargeAreaEQTransformer/REALEventFiles120/'
##in_inv_dir = '../LargeAreaEQTransformer/downloads_mseeds/StationXML/'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/LargeAreaEQTransformer/REALEventFiles120/'
#in_inv_dir = '../LargeAreaEQTransformer/StationXML/'
#out_calib_file = '../LargeAreaEQTransformer/REAL/Events/constrainwindow120_magnitude_calibration_20200107_20200114.txt'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/events_MATCH_eqt_2018_2020.txt'
#in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
##in_event_file_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/EventFiles/'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/EventFiles120/'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/StationXML/'
#out_calib_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/constrainwindow120_magnitude_calibration_2018_2020.txt'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/events_MATCH_eqt_2018_2020.txt'
#in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REALEventFiles120/'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/StationXML/'
#out_calib_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/constrainwindow120_magnitude_calibration_2018_2020.txt'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_EQT_20200107_20200114.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/EventFiles/'
#out_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20200107_20200114.txt'
#skip_stations = []

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_EQT_20200107_20200114.txt'
####in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/EventFiles/'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/'
#out_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20200107_20200114.txt'
#skip_stations = []

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_EQT_20191228_20200114.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/EventFiles/'
#out_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20191228_20200114.txt'
#skip_stations = ['UUPR']

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_EQT_20191228_20200114.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/EventFiles/'
#out_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20191228_20200114.txt'
#skip_stations = ['UUPR']

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_EQT_20180101_20210601.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/EventFiles/'
#out_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20180101_20210601.txt'
#skip_stations = ['CLB1', 'ZCPR']

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_EQT_20180101_20210601.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/EventFiles/'
#out_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20180101_20210601.txt'
#skip_stations = ['CLB1']

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_EQT_20180101_20211001.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/EventFiles/'
#out_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20180101_20211001.txt'
#skip_stations = []

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/us_events_MATCH_EQT_20180101_20220101.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/downloads_mseeds/'
#out_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20180101_20220101.txt'
#skip_stations = ['CLB1']

in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/StationXML/'
in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
in_match_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/us_events_MATCH_EQT_20180101_20220101.txt'
in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/downloads_mseeds/'
out_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20180101_20220101.txt'
skip_stations = []

min_lat = 17.6
max_lat = 18.3
min_lon = -67.3
max_lon = -66.4

# Get station data
stations_ = json.load(open(in_station_file))

# Get station inventory for instrument response
inv_map = utils_mag.get_station_inventory(in_inv_dir, stations_)

# Get catalog event data
map_cat_events = utils_mag.get_catalog_events_map(in_match_catalog_file, stations_)

# Get catalog phase data from HYPOINVERSE arc file
[event_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)

# Save these for distance correction calculation
all_log10_dist = []
all_peak_amp = []
all_cat_mag = []
all_list_ev_sta = []

order_sta = []
for sta in sorted(stations_.keys()):
   if sta in skip_stations:
      continue
   order_sta.append(sta)
print("len(order_sta): ", len(order_sta))

# Loop over catalog events
t0 = time.time()
for ev_id in map_cat_events:

   # Skip if event is located out of bounds (don't want to include faraway regional events) - CATALOG COMPARISON ONLY
   ev_lat = event_dict[ev_id]['event'][1]
   ev_lon = event_dict[ev_id]['event'][2]
   if ((ev_lat < min_lat) or (ev_lat > max_lat) or (ev_lon < min_lon) or (ev_lon > max_lon)):
      continue

   # Get event origin time
   ev_origin_time = event_dict[ev_id]['event'][0]

   # Get list of HYPOINVERSE phases for this event ev_id
   phases_list = set().union(event_dict[ev_id]['P'].keys(), event_dict[ev_id]['S'].keys())

   # Loop over stations for this event ev_id
   for ista,sta in enumerate(order_sta):

      # Calculate maximum amplitude at this station
      [mag_amp_term, flag_sta_amp] = utils_mag.calculate_max_amplitude_station(in_event_file_dir, event_dict, ev_id,
         ev_origin_time, sta, phases_list, inv_map)

      # This station will be used in ML calculation
      if (flag_sta_amp):
         all_log10_dist.append(map_cat_events[ev_id][4][sta]) # log10(dist_ev_sta_km)
         all_peak_amp.append(mag_amp_term)
         all_cat_mag.append(map_cat_events[ev_id][3]) # cat_mag
         all_list_ev_sta.append(sta)

print("Total runtime magnitude calculation: ", time.time()-t0)

# Perform the magnitude calibration: solve for mag_slope, mag_icpt
print("Check dimensions for distance correction arrays", len(all_log10_dist), len(all_peak_amp), len(all_cat_mag), len(all_list_ev_sta), len(order_sta))
all_log10_dist = np.asarray(all_log10_dist)
all_diff_mag = np.asarray(all_cat_mag) - np.asarray(all_peak_amp)
all_ev_sta = np.asarray(all_list_ev_sta)
design_matrix = all_log10_dist
for ista,sta in enumerate(order_sta):
   curr_ev_sta = np.zeros(np.shape(all_log10_dist))
   curr_ev_sta[np.where(all_ev_sta == sta)] = 1.0
   design_matrix = np.vstack([design_matrix, curr_ev_sta])

#AAA = np.vstack([all_log10_dist, np.ones(len(all_log10_dist))]).T
AAA = design_matrix.T
print("np.shape(AAA) = ", np.shape(AAA))
#mag_slope, mag_icpt = np.linalg.lstsq(AAA, all_diff_mag, rcond=None)[0]
#print("mag_slope = ", mag_slope, ", mag_icpt = ", mag_icpt)

# Normal equations for least squares (Eq. 2.3 from Aster)
A_T_A = np.matmul(design_matrix, AAA)
inv_A_T_A = np.linalg.inv(A_T_A)
#dim_ATA, dim_ATA2 = np.shape(A_T_A)
#iden = np.eye(dim_ATA, dim_ATA)
#inv_A_T_A = np.linalg.lstsq(A_T_A, iden)[0]
A_T_d = np.matmul(design_matrix, all_diff_mag)
print("np.shape(A_T_A) = ", np.shape(A_T_A), ", np.shape(A_T_d) = ", np.shape(A_T_d))

calib_param = np.matmul(inv_A_T_A, A_T_d)
print("np.shape(calib_param) = ", np.shape(calib_param))
print("calib_param = ", calib_param)

with open(out_calib_file, 'w') as fout:
   fout.write(('%s %4.8f\n') % ('k', calib_param[0]))
   for ista,sta in enumerate(order_sta):
      fout.write(('%s %4.8f\n') % (sta, calib_param[ista+1]))

