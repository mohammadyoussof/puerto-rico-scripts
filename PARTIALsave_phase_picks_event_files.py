from obspy import read
from obspy import UTCDateTime
from obspy.core.stream import Stream
from obspy.io.sac.sactrace import SACTrace
import sys
import json
import glob
import utils_hypoinverse as utils_hyp



if len(sys.argv) != 3:
   print("Usage: python PARTIALsave_phase_picks_event_files.py <start_ind> <end_ind>")
   sys.exit(1)

IND_FIRST = int(sys.argv[1])
IND_LAST = int(sys.argv[2])
print("PROCESSING:", IND_FIRST, IND_LAST)
   
# Inputs
in_hinv_arc_file = '../LargeAreaEQTransformer/association/merged_locate_pr.arc' # use the merged file
in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
in_event_dir = '../LargeAreaEQTransformer/EventFiles120Filtered/'
#in_mseed_dir = '../LargeAreaEQTransformer/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_locate_pr.arc' # use the merged file
#in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/downloads_mseeds/'
#wtime_before = 30 # time window before origin time (s)
#wtime_after = 60 # time window after origin time (s)
#wtime_before = 15 # time window before origin time (s)
#wtime_after = 45 # time window after origin time (s)
wtime_before = 15 # time window before origin time (s)
wtime_after = 105 # time window after origin time (s)

# Read in and store event and phase data from HYPOINVERSE arc file
[event_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)


print("\n ------------------- OUTPUT PLOTS --------------------------\n")
stations_ = json.load(open(in_station_file))
for kk in range(IND_FIRST, IND_LAST):
   curr_ev = event_dict[ev_id_list[kk]]

   # Get time window for plots
   ev_origin_time = curr_ev['event'][0]
   start_time = ev_origin_time - wtime_before
   end_time = ev_origin_time + wtime_after
   print(kk, ev_id_list[kk], ev_origin_time, start_time, end_time)

   # Read in time window for plots from daylong mseed files
   st_all = Stream()
   for sta,val in stations_.items():
      net = val['network']
      file_path = in_event_dir+str(ev_id_list[kk])+'/'
      file_names = glob.glob(file_path+'*'+sta+'*.sac')
      if (len(file_names) > 0):
         st = read(file_path+'*'+sta+'*.sac')
         st_all += st

#   print(st_all.__str__(extended=True))

   for ist in range(0,len(st_all)):

      file_path = in_event_dir+str(ev_id_list[kk])+'/'
      output_event_name = file_path+'event'+format(ev_id_list[kk],'06d')+'_'+ev_origin_time.strftime('%Y%m%dT%H%M%S.%f')

      # Loop over phase picks and plot them
      for netsta,val in curr_ev['P'].items():
         curr_net = netsta[0:2]
         curr_sta = netsta[2:]
         if ((curr_net == st_all[ist].stats.network) and (curr_sta == st_all[ist].stats.station)):
            ph_UTC_time = val[1]
            ph_rel_time = ph_UTC_time - st_all[ist].stats.starttime
            st_all[ist].stats.sac.a = ph_rel_time
#            print(st_all[ist].stats.sac.a)

      for netsta,val in curr_ev['S'].items():
         curr_net = netsta[0:2]
         curr_sta = netsta[2:]
         if ((curr_net == st_all[ist].stats.network) and (curr_sta == st_all[ist].stats.station)):
            ph_UTC_time = val[1]
            ph_rel_time = ph_UTC_time - st_all[ist].stats.starttime
            st_all[ist].stats.sac.t0 = ph_rel_time
#            print(st_all[ist].stats.sac.t0)

      output_file_name = output_event_name+'_'+st_all[ist].stats.network+'.'+st_all[ist].stats.station+'.'+st_all[ist].stats.channel+'.sac'
      sac_tr = SACTrace.from_obspy_trace(st_all[ist])
      sac_tr.write(output_file_name, headonly=True)

