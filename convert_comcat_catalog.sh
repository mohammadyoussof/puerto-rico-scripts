#!/bin/bash

# Convert catalog in USGS ComCat API format to a plain text catalog
#
# 2020-11-02	C. Yoon    First created

#in_comcat_file='../Catalog/test_catalog.txt'
#out_catalog_file='../Catalog/test_catalog_puerto_rico.txt'
#catalog_start_time='2020-01-07T00:00:00'

#in_comcat_file='../Catalog/catalog_puerto_rico_2018_2020.txt'
#out_catalog_file='../Catalog/catalog_new_puerto_rico_2018_2020.txt'
#catalog_start_time='2018-01-01T00:00:00'

#in_comcat_file='../Catalog/catalog_puerto_rico_2018_20191227.txt'
#out_catalog_file='../Catalog/catalog_new_puerto_rico_2018_20191227.txt'
#catalog_start_time='2018-01-01T00:00:00'

#in_comcat_file='../Catalog/catalog_puerto_rico_20191227_2020.txt'
#out_catalog_file='../Catalog/catalog_new_puerto_rico_20191227_2020.txt'
#catalog_start_time='2018-01-01T00:00:00'

in_comcat_file='../Catalog/catalog_puerto_rico_20200107_20200114.txt'
out_catalog_file='../Catalog/catalog_new_puerto_rico_20200107_20200114.txt'
catalog_start_time='2020-01-07T00:00:00'

# First column will have epoch time, then latitude, longitude, depth, magnitude, eventid
paste <(awk -F'|' 'NR>1 {system("gdate -d"$2" +%s")}' ${in_comcat_file}) <(awk -F'|' 'NR>1 {print $3, $4, $5, $11, $1}' ${in_comcat_file}) > tmp.txt

# Remove catalog_start_time from epoch time
catalog_start_epoch=`gdate -d${catalog_start_time} +%s`
echo ${catalog_start_epoch}

# Output plain text catalog with columns: time(sec) since catalog_start_time, latitude, longitude, depth, magnitude, eventid
awk -v ctime=${catalog_start_epoch} '{printf "%15.5f %8.5f %8.5f %6.4f %5.3f %s\n", $1-ctime, $2, $3, $4, $5, $6}' tmp.txt > ${out_catalog_file}
rm tmp.txt

