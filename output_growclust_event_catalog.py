import datetime

# Write out relocated GrowClust event catalogs for plotting in GMT
# [Must have run convert_hyposvi_to_catalog.py before to get the input magnitude map]

#catalog_start_time = '2020-01-07T00:00:00'
#in_growclust_file = '../LargeAreaEQTransformer/GrowClust/OUT/out.growclust_cat'
#out_growclust_file = '../Catalog/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114.txt'
#in_growclust_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/GrowClust/OUT_CC060/out.growclust_cat'
#out_growclust_file = '../Catalog/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114_maxdt2.txt'
#in_growclust_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/GrowClust/OUT_CC070/out.growclust_cat'
#out_growclust_file = '../Catalog/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114_maxdt2_cc070.txt'

#in_growclust_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/GrowClust/OUT_CC060/out.growclust_cat'
#out_growclust_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_GrowClust_maxdt2_cc060.txt'

#in_growclust_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/GrowClust/OUT_VELPRSN/out.growclust_cat'
#in_magmap_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOSVI_VELPRSN_MagnitudeMap.txt'
#out_growclust_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_GrowClust_VELPRSN_maxdt2.txt'

#in_growclust_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/GrowClust/OUT_VELZHANG/out.growclust_cat'
#in_magmap_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOSVI_VELZHANG_MagnitudeMap.txt'
#out_growclust_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_GrowClust_VELZHANG_maxdt2.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_growclust_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/GrowClust/OUT_VELZHANG/out.growclust_cat'
#in_magmap_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOSVI_VELZHANG_MagnitudeMap.txt'
#out_growclust_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_GrowClust_VELZHANG_maxdt2.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_growclust_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/GrowClust/OUT_VELZHANG/out.growclust_cat'
#in_magmap_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOSVI_VELZHANG_MagnitudeMap.txt'
#out_growclust_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_GrowClust_VELZHANG_maxdt2.txt'
#out_nonclust_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_NonCluster_GrowClust_VELZHANG_maxdt2.txt'

#catalog_start_time = '2018-01-01T00:00:00'
#in_growclust_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/GrowClust/OUT_EQT_20180101_20220101_VELPRSN/out.growclust_cat'
#in_magmap_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELPRSN_MagnitudeMap.txt'
#out_growclust_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_GrowClust_VELPRSN_maxdt2.txt'
#out_nonclust_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELPRSN_maxdt2.txt'

catalog_start_time = '2018-01-01T00:00:00'
in_growclust_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/GrowClust/OUT_EQT_20180101_20220101_VELZHANG/out.growclust_cat'
in_magmap_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_MagnitudeMap.txt'
out_growclust_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_maxdt2.txt'
out_nonclust_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_maxdt2.txt'

# Read in magnitude map
mag_map = {}
with open(in_magmap_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      hsvi_ev_id = int(split_line[0])
      hinv_mag = float(split_line[3])
      mag_map[hsvi_ev_id] = hinv_mag

# Output growclust file with magnitudes
catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
fout = open(out_growclust_file, 'w')
fout_nonclust = open(out_nonclust_file, 'w')
with open(in_growclust_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      year = int(split_line[0])
      month = int(split_line[1])
      day = int(split_line[2])
      hour = int(split_line[3])
      minute = int(split_line[4])
      second = float(split_line[5])
      if (second >= 60.0):
         print("BEFORE: ", year, month, day, hour, minute, second)
         second -= 60.0
         minute += 1
         print("AFTER: ", year, month, day, hour, minute, second)
      origin_time_nosec = datetime.datetime(year, month, day, hour, minute)
      origin_delta = datetime.timedelta(seconds=second)
      origin_time = origin_time_nosec + origin_delta
      num_sec = (origin_time - catalog_ref_time).total_seconds()
      lat_deg = split_line[7]
      lon_deg = split_line[8]
      depth_km = split_line[9]
      evid = int(split_line[6])
      mag = mag_map[evid]
      nbranch = int(split_line[13]) # if nbranch==1 then not relocated, skip this event
      if (nbranch != 1):
         fout.write(("%f %s %s %s %s %d\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
      else:
         fout_nonclust.write(("%f %s %s %s %s %d\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
fout.close()
fout_nonclust.close()
