import json
import os

# Helper script to copy mseed files into another subdirectory

#first_day = '20200106'
#first_day = '20200105'
#first_day = '20200104'
#first_day = '20200103'
#first_day = '20200102'
first_day = '20200101'
dest_day_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/test_mseeds_20200107/'
in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'

stations_ = json.load(open(in_station_file))
for sta,val in stations_.items():
   src_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/downloads_mseeds/'+sta+'/*__'+first_day+'T000000Z__*'
   dest_dir = dest_day_dir+sta+'/'
   os.system('install -D '+src_dir+' '+dest_dir)
