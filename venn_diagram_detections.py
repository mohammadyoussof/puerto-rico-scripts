import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib_venn import venn2, venn2_circles # conda install matplotlib-venn

rcParams.update({'font.size': 20})
rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"


## Subset sizes - LargeAreaEQTransformer
#s = (
#      4,  # Ab
#      80,  # aB
#      16,  # AB
#)

# Subset sizes - FullEQTransformer
s = (
      4,  # Ab
      85,  # aB
      11,  # AB
)

#v = venn2(subsets=s, set_labels=('Catalog:\n 1,871 events', 'EQTransformer:\n 9,038 events'))
v = venn2(subsets=s, set_labels=('Catalog:\n 16,876 events', 'EQTransformer:\n 111,458 events'))

# Subset labels
#v.get_label_by_id('10').set_text('358\nCatalog\nonly\nevents')
#v.get_label_by_id('01').set_text('7,525\nEQTransformer\nonly\nevents')
#v.get_label_by_id('11').set_text('1,513\noverlapping\nevents')
v.get_label_by_id('10').set_text('4,526\nCatalog\nonly\nevents')
v.get_label_by_id('01').set_text('99,108\nEQTransformer\nonly\nevents')
v.get_label_by_id('11').set_text('12,350\noverlapping\nevents')

# Subset colors
v.get_patch_by_id('10').set_color('red')
v.get_patch_by_id('01').set_color('cyan')
v.get_patch_by_id('11').set_color('blue')

# Subset alphas
v.get_patch_by_id('10').set_alpha(0.4)
v.get_patch_by_id('01').set_alpha(1.0)
v.get_patch_by_id('11').set_alpha(0.7)

# Border styles
c = venn2_circles(subsets=s, linestyle='solid')
#c[0].set_ls('dashed')  # Line style
#c[0].set_lw(2.0)       # Line width


#plt.title('Total: 9,396 earthquakes')
plt.title('Total: 115,984 earthquakes')

out_dir = '../Plots/'
#plt.savefig(out_dir+'venn_diagram_LargeAreaEQTransformer_detections.pdf')
plt.savefig(out_dir+'venn_diagram_FullEQTransformer_detections.pdf')

