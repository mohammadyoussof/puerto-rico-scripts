#!/usr/bin/env bash
#               GMT ANIMATION 02
#
# Purpose:      Make simple animated GIF of an illuminated DEM grid
# GMT modules   math, makecpt, grdimage, plot, movie
# Unix progs:   cat
# Note:         Run with any argument to build movie; otherwise 1st frame is plotted only.

if [ $# -eq 0 ]; then   # Just make master PostScript frame 0
   opt="-Mps -Fnone"
else  # Make animated GIF
#   opt="-A+l"
   opt="-A" # no infinite loop
fi

# 1. Create files needed in the loop
cat << EOF > pre.sh
gmt begin
   gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
   gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

#   NDAY=7
#   DELTA_DAY=0.02
##   gmt math -T0/\${NDAY}/\${DELTA_DAY} T \${DELTA_DAY} ADD = timeframes.txt
#   gmt math -T0/\${NDAY}/\${DELTA_DAY} T 0.5 ADD = timeframes.txt

   NDAY=1095
   DELTA_DAY=1
#   gmt math -T0/\${NDAY}/\${DELTA_DAY} T \${DELTA_DAY} ADD = timeframes.txt
#   gmt math -T0/\${NDAY}/\${DELTA_DAY} T 1 ADD = timeframes.txt
   gmt math -T0/\${NDAY}/\${DELTA_DAY} T 7 ADD = timeframes.txt

   out_color_filename=depthcolors.cpt
   out_color_file=/Users/cyoon/Documents/PuertoRico/Plots/\${out_color_filename}
   gmt makecpt -Cinferno -I -T0/20/0.01 -H > \${out_color_file}
   gmt colorbar -C\${out_color_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Depth (km)"

   min_lat=17.6
   max_lat=18.3
   min_lon=-67.3
   max_lon=-66.4

   reg=-R\${min_lon}/\${max_lon}/\${min_lat}/\${max_lat}
   proj=-JM16
   echo \${reg}
   lat_lon_spacing=0.1

   gmt basemap \${proj} \${reg} -Ba\${lat_lon_spacing}fg -BneWS -U
   gmt coast \${proj} \${reg} -W0.25p,black -Na -G255 -Sdeepskyblue -Lg-66.6/17.65+c-66.5/17.65+w20+f+lkm

   in_sta_file=/Users/cyoon/Documents/PuertoRico/Catalog/FullEQTransformer_pr_stations_large.txt
   awk '{print \$2 " " \$3 " " \$4}' \${in_sta_file} | gmt plot \${proj} \${reg} -Si0.4 -W0.1c,black -Ggray -:

gmt end
EOF

# 2. Set up the main frame script
cat << EOF > main.sh
gmt begin
   out_color_filename=depthcolors.cpt
   out_color_file=/Users/cyoon/Documents/PuertoRico/Plots/\${out_color_filename}
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/catalog_new_puerto_rico_20200107_20200114.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/catalog_new_puerto_rico_2018_2020.txt
   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt

   min_lat=17.6
   max_lat=18.3
   min_lon=-67.3
   max_lon=-66.4

   reg=-R\${min_lon}/\${max_lon}/\${min_lat}/\${max_lat}
   proj=-JM16

#   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$1/86400.) >= FD && (\$1/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$2, \$3, \$4, \$5*\$5*0.01 + 0.0200}' \${in_event_file} | gmt plot \${proj} \${reg} -Sc -W0.01c -G255 -BneWS -: -C\${out_color_file}
   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$1/86400.) >= FD && (\$1/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$2, \$3, \$4, \$5*\$5*0.01 + 0.0200}' \${in_event_file} | gmt plot \${proj} \${reg} -Sc -W0.01c -G255 -BneWS -: -C\${out_color_file}
gmt end
EOF

# 3. Run the movie
#gmt movie main.sh -C10ix8ix72 -Nmovie_small_map_puerto_rico -Ttimeframes.txt -Sbpre.sh -D6 -Zs $opt

#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_small_map_puerto_rico -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C10ix8ix72 -Nmovie_small_map_puerto_rico_slices -Ttimeframes.txt -Sbpre.sh -D6 $opt

#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_LargeAreaEQTransformer_HYPOINVERSE -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_LargeAreaEQTransformer_HYPOINVERSE_slices -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt

#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_small_map_puerto_rico_2018_2020 -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_small_map_puerto_rico_2018_2020_slices -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt

#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_FullEQTransformer_HYPOINVERSE -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_FullEQTransformer_HYPOINVERSE_slices -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_FullEQTransformer_HYPOINVERSE_slices7days -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
