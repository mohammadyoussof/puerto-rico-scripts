import json
import os
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import utils_hypoinverse as utils_hyp
import utils_magnitude as utils_mag
from matplotlib import rcParams

matplotlib.use('Agg')
rcParams.update({'font.size': 24})
rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"


# Compute local magnitude (Wood-Anderson) for EQTransformer events, following magnitude calibration from missed catalog events

#in_hinv_arc_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_missed_catalog_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/events_MISSED_eqt_evid_20200107_20200114.txt'
#in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
#in_calib_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/constrainwindow120_magnitude_calibration_20200107_20200114.txt'
##in_event_file_dir = '../LargeAreaEQTransformer/20200107_20200114_Model1/MissedREALEventFiles120/'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/LargeAreaEQTransformer/20200107_20200114_Model1/MissedREALEventFiles120/'
#in_inv_dir = '../LargeAreaEQTransformer/StationXML/'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/events_MISSED_eqt_evid_2018_2020.txt'
#in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/station_list_edited_20180101_20201101.json'
#in_calib_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/constrainwindow120_magnitude_calibration_2018_2020.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/MissedREALEventFiles120/'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/StationXML/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_EQT_EVID_20200107_20200114.txt'
#in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20200107_20200114.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_EQT_EVID_20200107_20200114.txt'
#in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20200107_20200114.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_EQT_EVID_20191228_20200114.txt'
#in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20191228_20200114.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_EQT_EVID_20191228_20200114.txt'
#in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20191228_20200114.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_EQT_EVID_20180101_20210601.txt'
#in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20180101_20210601.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_EQT_EVID_20180101_20210601.txt'
#in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20180101_20210601.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_EQT_EVID_20180101_20211001.txt'
#in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20180101_20211001.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/MissedEventFiles/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
#in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/StationXML/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_magmiss_locate_pr.arc' # use the merged file
#in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_magcat_EQT_EVID_20180101_20220101.txt'
#in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/magnitude_calibration_us_EQT_20180101_20220101.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/downloads_mseeds/'

in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
in_inv_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/StationXML/'
in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
in_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_magcat_EQT_EVID_20180101_20220101.txt'
in_calib_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/magnitude_calibration_us_EQT_20180101_20220101.txt'
in_event_file_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/downloads_mseeds/'

#out_mag_plot_dir = '../LargeAreaEQTransformer/20200107_20200114_Model1/MagPlots/'
#out_calib_plot_file = 'real_constrainwindow120_FULL_median_calib_NOFILTERPHASETEST_2018_2020_catalog_missed_event_mag_comparison.png'
#out_missed_catalog_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/events_MISSED_magcat_eqt_evid_20200107_20200114.txt'
#out_hinv_arc_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/MagPlots/'
#out_calib_plot_file = 'real_constrainwindow120_FULL_median_calib_NOFILTERPHASETEST_2018_2020_catalog_missed_event_mag_comparison.png'
#out_missed_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/events_MISSED_magcat_eqt_evid_2018_2020.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20200107_20200114_missed.pdf'
#out_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_magcat_EQT_EVID_20200107_20200114.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20200107_20200114_missed.pdf'
#out_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_magcat_EQT_EVID_20200107_20200114.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20191228_20200114_missed.pdf'
#out_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_magcat_EQT_EVID_20191228_20200114.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20191228_20200114_missed.pdf'
#out_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_magcat_EQT_EVID_20191228_20200114.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20180101_20210601_missed.pdf'
#out_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_magcat_EQT_EVID_20180101_20210601.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20180101_20210601_missed.pdf'
#out_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_magcat_EQT_EVID_20180101_20210601.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20180101_20211001_missed.pdf'
#out_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_magcat_EQT_EVID_20180101_20211001.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

#out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/MagPlots/'
#out_calib_plot_file = 'magnitude_calibration_us_EQT_20180101_20220101_missed.pdf'
#out_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_magcat_EQT_EVID_20180101_20220101_comparison.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/MagPlots/'
out_calib_plot_file = 'magnitude_calibration_us_EQT_20180101_20220101_missed.pdf'
out_missed_catalog_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_magcat_EQT_EVID_20180101_20220101_comparison.txt'
out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

min_lat = 17.6
max_lat = 18.3
min_lon = -67.3
max_lon = -66.4

# Get station data
stations_ = json.load(open(in_station_file))

# Get station inventory for instrument response
inv_map = utils_mag.get_station_inventory(in_inv_dir, stations_)

# Get all event data from HYPOINVERSE arc file
map_events = utils_mag.get_events_map(in_hinv_arc_file, stations_)
map_cat_events = utils_mag.get_missed_catalog_events_map(in_missed_catalog_file, stations_)

# Get all event phase data from HYPOINVERSE arc file
[event_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)

# Distance correction, slope and intercept - get this from missed catalog events
# Run calibrate_local_magnitude_wood_anderson.py
#   dist_corr_slope =  0.725917698378 # TEST 20200107-20200114
#   dist_corr_icpt =  3.76802638352 # TEST 20200107-20200114
#   dist_corr_slope = 1.38657435857 # PHASETEST 20200107-20200114
#   dist_corr_icpt = 2.58957042452 # PHASETEST 20200107-20200114
#dist_corr_slope = 1.41556597881 # NOFILTERPHASETEST 20200107-20200114
#dist_corr_icpt = 2.49115637769 # NOFILTERPHASETEST 20200107-20200114
#dist_corr_slope = 1.18039588465 # NOFILTERPHASETEST 20200107-20200114 StationXML
#dist_corr_icpt = 3.00651850763 # NOFILTERPHASETEST 20200107-20200114 StationXML

# Distance correction, slope (common) and intercept (per station) - get this from missed catalog events
order_sta = []
dist_corr_icpt = []
with open(in_calib_file, 'r') as fin:
   for line in fin:
      if (line[0] == 'k'):
         dist_corr_slope = float(line.split()[1])
      else:
         split_line = line.split()
         order_sta.append(split_line[0])
         dist_corr_icpt.append(float(split_line[1]))

# Loop over all events
t0 = time.time()
for ev_id in map_events:

#for ev_id in map_cat_events:
#   # Skip if event is located out of bounds (don't want to include faraway regional events) - CATALOG COMPARISON ONLY
#   ev_lat = event_dict[ev_id]['event'][1]
#   ev_lon = event_dict[ev_id]['event'][2]
#   if ((ev_lat < min_lat) or (ev_lat > max_lat) or (ev_lon < min_lon) or (ev_lon > max_lon)):
#      continue

   median_ML_Richter = utils_mag.calculate_local_magnitude(in_event_file_dir, event_dict, ev_id, order_sta, map_events,
      inv_map, dist_corr_slope, dist_corr_icpt)
   map_events[ev_id].append(median_ML_Richter) # ML is the last item in the map values
print("Total runtime magnitude calculation: ", time.time()-t0)

# After magnitude calibration, show that magnitudes match for catalog events only: save arrays for plots
all_catalog_mag = []
all_catalog_ML = []
for ev_id in map_events: # Loop over catalog events only
   if (ev_id in map_cat_events):
      if (len(map_events[ev_id]) > 6): # ML exists for this event, last item in map for all events
         all_catalog_mag.append(map_cat_events[ev_id][3]) # Get magnitude from catalog
         all_catalog_ML.append(map_events[ev_id][6]) # Get ML from map for all events

# After magnitude calibration, show that magnitudes match: plot catalog magnitude vs calculated ML
equal_line = np.linspace(-1, 7, 100)
plt.figure(figsize=(6,8))
plt.scatter(all_catalog_mag, np.asarray(all_catalog_ML), facecolors='none', edgecolors='k', linewidths=0.1)
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlim([0, 7])
plt.ylim([0, 7])
plt.xticks([0, 1, 2, 3, 4, 5, 6, 7])
plt.yticks([0, 1, 2, 3, 4, 5, 6, 7])
plt.xlabel('Catalog magnitude $M_{cat}$')
plt.ylabel('Local magnitude $M_{L}$')
plt.tight_layout()
if not os.path.exists(out_mag_plot_dir):
   os.makedirs(out_mag_plot_dir)
plt.savefig(out_mag_plot_dir+out_calib_plot_file)

# Output magnitudes to file: missed catalog events
fout_missed = open(out_missed_catalog_file, 'w')
with open(in_missed_catalog_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      ev_id = int(split_line[6])
      if (ev_id in map_events):
         ev_mag = 0.0
         if (len(map_events[ev_id]) > 6): # ML exists for this event, last item in map for all events
            ev_mag = map_events[ev_id][6]
         fout_missed.write(('%s %3.2f\n') % (line.replace('\n',''), ev_mag))
fout_missed.close()

# Output magnitudes to HYPOINVERSE files: all events
fout_arc = open(out_hinv_arc_file, 'w')
fout_sum = open(out_hinv_sum_file, 'w')
with open(in_hinv_arc_file, 'r') as fin:
   for line in fin:
      if ((line[0:2] == '19') or (line[0:2] == '20')): # Replace magnitude in line
         ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line))
         if (ev_id in map_events):
            ev_mag = 0.0
            if (len(map_events[ev_id]) > 6): # ML exists for this event, last item in map for all events
               ev_mag = int(round(100*map_events[ev_id][6]))
            fout_arc.write(('%s%3d%s') % (line[0:147], ev_mag, line[150:]))
            fout_sum.write(('%s%3d%s') % (line[0:147], ev_mag, line[150:]))
      else:
         if (ev_id in map_events):
            fout_arc.write(('%s') % line)
fout_arc.close()
fout_sum.close()

