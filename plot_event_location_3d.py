import numpy as np
import json
import os
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.markers as mk
import matplotlib.cm as cm
import matplotlib.colors as colors
from matplotlib import rcParams

rcParams.update({'font.size': 16})
rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"


in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
#in_event_file = '../Catalog/catalog_new_puerto_rico_20200107_20200114.txt'
#in_event_file = '../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt'
in_event_file = '../Catalog/LargeAreaEQTransformer_HYPODD_puerto_rico_catalog_20200107_20200114.txt'
#in_event_file = '../Catalog/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114.txt'
#in_event_file = '../Catalog/catalog_new_puerto_rico_2018_2020.txt'
#in_event_file = '../Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt'
flag_movie = True

#out_movie_dir = '../Plots/movie_LargeAreaEQTransformer_HYPOINVERSE/'
out_movie_dir = '../Plots/movie_LargeAreaEQTransformer_HYPODD/'
if not os.path.exists(out_movie_dir):
   os.makedirs(out_movie_dir)


# Get station data
stations_ = json.load(open(in_station_file))

# Read in event data
[times, lat, lon, depth, mag] = np.loadtxt(in_event_file, usecols=(0,1,2,3,4), unpack=True)
day = times/86400.
print("len(times) = ", len(times))

depth = -1*depth
#mag_markers = 2*np.multiply(mag, mag)
mag_markers = 0.2


## Read in station locations
#[sta_lat, sta_lon, sta_depth] = np.loadtxt(input_dir+"threestations.dat", unpack=True)
#sta_depth = -1*sta_depth
#sta_lon = sta_lon-360


fig = plt.figure(figsize=(16,12))
ax = fig.add_subplot(111, projection='3d')

# Plot event locations
###p = ax.scatter(lon, lat, zs=depth, s=mag_markers, c=depth, vmin=-20, vmax=0) # color by depth
#p = ax.scatter(lon, lat, zs=depth, s=mag_markers, c=day, vmin=0, vmax=7, cmap='viridis', facecolors='none') # color by time (days since 20200107)
#p = ax.scatter(lon, lat, zs=depth, s=mag_markers, c=day, vmin=0, vmax=1095, cmap='viridis', facecolors='none') # color by time (days since 20180101)
#p = ax.scatter(lon, lat, zs=depth, s=mag_markers, c=day, vmin=699, vmax=1095, cmap='viridis', facecolors='none') # color by time (days since 20191201)
###p = ax.scatter(lon, lat, zs=depth, s=mag_markers, c=day, vmin=40, vmax=70, cmap='jet_r') # color by time (days since 20100601) - Cluster #1

# Snapshots in time
#FIRST_DAY = 0 
#LAST_DAY = 9
#ind_day = np.where(np.logical_and(day >= FIRST_DAY, day <= LAST_DAY)) 
#p = ax.scatter(lon[ind_day], lat[ind_day], zs=depth[ind_day], s=mag_markers[ind_day], c=depth[ind_day], vmin=-10, vmax=0) # color by depth

#cbar = fig.colorbar(p)
###cbar.set_label("Depth (km)") # color by depth
#cbar.set_label("Days since 2020-01-07") # color by time (days since 20200107)
#cbar.set_label("Days since 2018-01-01") # color by time (days since 20180101)

## Plot station locations
#ax.scatter(sta_lon, sta_lat, zs=sta_depth, s=150, c="black", marker="^")
#sta_names = ["WHAR", "ARK1", "ARK2"]
#for i in range(len(sta_names)):
#   ax.text(sta_lon[i], sta_lat[i], sta_depth[i], sta_names[i], size=15)

ax.set_xlabel('Longitude (deg)', labelpad=15)
ax.set_ylabel('Latitude (deg)', labelpad=10)
ax.set_zlabel('Depth (km)', labelpad=10)

ax.set_xlim3d([-67.3, -66.4])
ax.set_ylim3d([17.6, 18.3])
ax.set_zlim3d([-20, 0])
plt.xticks([-67.3, -67.0, -66.7, -66.4])
plt.yticks([17.6, 17.8, 18.0, 18.2])

ax.tick_params(axis='x', pad=7)
ax.tick_params(axis='y', pad=2)

ax.view_init(elev=30.0, azim=-108)

if (flag_movie):
   # Test movie (comment out plt.show())
   days_per_frame = 7 / 360.
   for ii in range(0,360,1):
      ax.view_init(elev=23.0, azim=ii)
#      p = ax.scatter(lon, lat, zs=depth, s=mag_markers, c=day, vmin=0, vmax=7, cmap='viridis', facecolors='none') # color by time (days since 20200107)

      # --- EVOLUTION MOVIE ---
      curr_frame = ii * days_per_frame
      ind_select = (day >= (ii*days_per_frame)) & (day <= ((ii+1)*days_per_frame))
      print(ii, curr_frame, np.where(ind_select))
      p = ax.scatter(lon[ind_select], lat[ind_select], zs=depth[ind_select], s=mag_markers, c=day[ind_select], vmin=0, vmax=7, cmap='viridis', facecolors='none') # color by time (days since 20200107)
      if (ii == 0):
         cbar = fig.colorbar(p)
         cbar.set_label("Days since 2020-01-07") # color by time (days since 20200107)
      # --- EVOLUTION MOVIE ---

      plt.savefig(out_movie_dir+"movie"+str(ii).zfill(3)+".png")

   ## After this script, run:
   # $ ffmpeg -framerate 24 -i movie%03d.png -vcodec libx264 -pix_fmt yuv420p movie.mp4
   # $ ffmpeg -i movie.mp4 -filter:v "setpts=3.0*PTS" movie_final.mp4
else:
   plt.show()

