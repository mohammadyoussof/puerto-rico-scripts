#!/bin/bash

# Plot Puerto Rico earthquakes on a map

#gmt gmtset BASEMAP_TYPE plain
#gmt gmtset HEADER_FONT_SIZE 24p
#gmt gmtset LABEL_FONT_SIZE 16p # smaller font for labels
##gmt gmtset PLOT_DEGREE_FORMAT +DF
#gmt gmtset PLOT_DEGREE_FORMAT -DF
#gmt gmtset GRID_PEN_PRIMARY 0.1p
#
gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

gmt gmtset LABEL_FONT_SIZE 24p #poster
gmt gmtset FONT_ANNOT 24p #poster

#in_sta_file=../Catalog/FullEQTransformer_pr_stations_large.txt
#in_temp_sta_file=../Catalog/FullEQTransformer_pr_stations_large_temporary.txt
#in_IU_sta_file=../Catalog/FullEQTransformer_pr_stations_large_IU.txt
#out_map_sta_file=../Plots/FullEQTransformer_pr_stations_large

in_temp_sta_file=../Catalog/FullEQTransformer_pr_stations_large_temporary.txt
in_IU_sta_file=../Catalog/FullEQTransformer_pr_stations_large_IU.txt
in_sta_file=../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_pr_stations.txt

#out_map_sta_file=../Plots/EQT_20180101_20210601_pr_stations_map
#out_map_sta_file=../EQT_20180101_20210601/Plots/EQT_20180101_20210601_pr_stations_map
#out_map_sta_file=../EQT_20180101_20211001/Plots/EQT_20180101_20211001_pr_stations_map
out_map_sta_file=../EQT_20180101_20220101/Plots/EQT_20180101_20220101_pr_stations_map

min_lat=17
max_lat=19
min_lon=-68
max_lon=-65

#min_lat=17.5
#max_lat=18.5
#min_lon=-67.5
#max_lon=-66.0

minbox_lat=17.6
maxbox_lat=18.3
minbox_lon=-67.3
maxbox_lon=-66.4

reg=-R${min_lon}/${max_lon}/${min_lat}/${max_lat}
proj=-JM16
echo ${reg}
lat_lon_spacing=0.5

region_inset=-R-75/-62/15/22
projection_inset=-JM2.0i



gmt begin ${out_map_sta_file}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}fg -BneWS #-U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G255 -Slightskyblue -Lg-65.8/17.35+c-65.7/17.35+w100+f+lkm
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Gwhite -:
   awk '{print $2 " " $3 " " $4}' ${in_temp_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,blue -Gyellow -:
   awk '{print $2 " " $3 " " $4}' ${in_IU_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,magenta -Gyellow -:
   gmt plot ${proj} ${reg} -W2,red << EOF
${minbox_lon} ${minbox_lat}
${minbox_lon} ${maxbox_lat}
${maxbox_lon} ${maxbox_lat}
${maxbox_lon} ${minbox_lat}
${minbox_lon} ${minbox_lat}
EOF
   gmt plot ${proj} ${reg} -W2,blue << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,blue << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

