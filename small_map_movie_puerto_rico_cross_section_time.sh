#!/usr/bin/env bash
#               GMT ANIMATION 02
#
# Purpose:      Make simple animated GIF of an illuminated DEM grid
# GMT modules   math, makecpt, grdimage, plot, movie
# Unix progs:   cat
# Note:         Run with any argument to build movie; otherwise 1st frame is plotted only.

if [ $# -eq 0 ]; then   # Just make master PostScript frame 0
   opt="-Mps -Fnone"
else  # Make animated GIF
#   opt="-A+l"
   opt="-A" # no infinite loop
fi

# 1. Create files needed in the loop
cat << EOF > pre.sh
gmt begin
   gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
   gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

#   NDAY=7
#   DELTA_DAY=0.02
##   gmt math -T0/\${NDAY}/\${DELTA_DAY} T \${DELTA_DAY} ADD = timeframes.txt
#   gmt math -T0/\${NDAY}/\${DELTA_DAY} T 0.5 ADD = timeframes.txt

   NDAY=1035
   DELTA_DAY=1
#   gmt math -T0/\${NDAY}/\${DELTA_DAY} T \${DELTA_DAY} ADD = timeframes.txt
#   gmt math -T0/\${NDAY}/\${DELTA_DAY} T 1 ADD = timeframes.txt
#   gmt math -T0/\${NDAY}/\${DELTA_DAY} T 7 ADD = timeframes.txt
   gmt math -T726/\${NDAY}/\${DELTA_DAY} T 7 ADD = timeframes.txt

   out_color_time_filename=timecolors.cpt
   out_color_time_file=/Users/cyoon/Documents/PuertoRico/Plots/\${out_color_time_filename}
   gmt makecpt -Cviridis -I -T726/1035/0.01 -H > \${out_color_time_file}
   start_date=2018-01-01

#   strike1_bproj=-JX30/15
#   strike1_brange=-R-30/30/-30/0
#   gmt basemap \${strike1_bproj} \${strike1_brange} -Ba10f5 -BneWS #-U
##   gmt colorbar -C\${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "\${start_date}

   strike2_bproj=-JX40/15
   strike2_brange=-R-40/40/-30/0
   gmt basemap \${strike2_bproj} \${strike2_brange} -Ba10f5 -BneWS #-U
#   gmt colorbar -C\${out_color_time_file} -Dx15.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "\${start_date}
gmt end
EOF

# 2. Set up the main frame script
cat << EOF > main.sh
gmt begin
   out_color_time_filename=timecolors.cpt
   out_color_time_file=/Users/cyoon/Documents/PuertoRico/Plots/\${out_color_time_filename}

#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/catalog_new_puerto_rico_20200107_20200114.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/catalog_new_puerto_rico_2018_2020.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_catalog_2018_2020.txt

##   in_event_file=/Users/cyoon/Documents/PuertoRico/Plots/puerto_rico_eqt_growclust_depth_20200107_20200114_small_map_large_seismicity_time_projection15.txt
##   in_event_file=/Users/cyoon/Documents/PuertoRico/Plots/puerto_rico_eqt_hinv_depth_2018_2020_small_map_large_seismicity_time_projection15.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Plots/FullEQTransformer_REAL_HYPOSVI_puerto_rico_catalog_combined_2018_2020_depth_0_1035_seismicity_time_projection15.txt
#   strike1_bproj=-JX30/15
#   strike1_brange=-R-30/30/-30/0
##   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$7, \$3*-1., \$6/86400., \$5*\$5*0.01 + 0.0200}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C\${out_color_time_file}
##   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$7, \$3*-1., \$6/86400., \$5*\$5*0.01 + 0.0200}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C\${out_color_time_file}
#   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$11, \$3*-1., \$6/86400., \$10}' \${in_event_file} | gmt plot \${strike1_bproj} \${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C\${out_color_time_file}

#   in_event_file=/Users/cyoon/Documents/PuertoRico/Plots/puerto_rico_eqt_growclust_depth_20200107_20200114_small_map_large_seismicity_time_projection95.txt
#   in_event_file=/Users/cyoon/Documents/PuertoRico/Plots/puerto_rico_eqt_hinv_depth_2018_2020_small_map_large_seismicity_time_projection95.txt
   in_event_file=/Users/cyoon/Documents/PuertoRico/Plots/FullEQTransformer_REAL_HYPOSVI_puerto_rico_catalog_combined_2018_2020_depth_0_1035_seismicity_time_projection95.txt
   strike2_bproj=-JX40/15
   strike2_brange=-R-40/40/-30/0
#   awk -v FD="0" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$7, \$3*-1., \$6/86400., \$5*\$5*0.01 + 0.0200}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C\${out_color_time_file}
#   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$7, \$3*-1., \$6/86400., \$5*\$5*0.01 + 0.0200}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C\${out_color_time_file}
   awk -v FD="\${MOVIE_COL0}" -v LD="\${MOVIE_COL1}" '{if ((\$6/86400.) >= FD && (\$6/86400.) <= LD) printf "%10.8f %10.8f %10.8f %6.4f\n", \$11, \$3*-1., \$6/86400., \$10}' \${in_event_file} | gmt plot \${strike2_bproj} \${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C\${out_color_time_file}
gmt end
EOF

# 3. Run the movie
#gmt movie main.sh -C10ix8ix72 -Nmovie_small_map_puerto_rico -Ttimeframes.txt -Sbpre.sh -D6 -Zs $opt

#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_small_map_puerto_rico -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C10ix8ix72 -Nmovie_small_map_puerto_rico_slices -Ttimeframes.txt -Sbpre.sh -D6 $opt

#gmt movie main.sh -C8.8ix6.8ix72 -NTEST_movie_LargeAreaEQTransformer_HYPOINVERSE -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_LargeAreaEQTransformer_HYPOINVERSE_slices -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt

#gmt movie main.sh -C14.0ix7.3ix72 -NTEST_movie_LargeArea_growclust_xsec_strike15 -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C18.0ix7.3ix72 -NTEST_movie_LargeArea_growclust_xsec_strike95 -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt

#gmt movie main.sh -C1080p -Nmovie_FullEQTransformer_HYPOSVI_slices7days_xsec_strike15 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -C14.0ix7.3ix120 -Nmovie_FullEQTransformer_HYPOSVI_slices7days_xsec_strike15 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
gmt movie main.sh -C18.0ix7.3ix120 -Nmovie_FullEQTransformer_HYPOSVI_slices7days_xsec_strike95 -Ttimeframes.txt -Sbpre.sh -D6 -Pe+ap $opt
#gmt movie main.sh -C14.0ix7.3ix72 -NTEST_movie_Full_HYPOINVERSE_xsec_strike15 -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C18.0ix7.3ix72 -NTEST_movie_Full_HYPOINVERSE_xsec_strike95 -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt

#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_small_map_puerto_rico_2018_2020 -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_small_map_puerto_rico_2018_2020_slices -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt

#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_FullEQTransformer_HYPOINVERSE -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_FullEQTransformer_HYPOINVERSE_slices -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
#gmt movie main.sh -C8.8ix6.8ix72 -Nmovie_FullEQTransformer_HYPOINVERSE_slices7days -Ttimeframes.txt -Sbpre.sh -D6 -Pb+a $opt
