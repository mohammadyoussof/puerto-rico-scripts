#from obspy.io.quakeml.core import Unpickler 
from obspy.core.event import read_events

#https://docs.obspy.org/packages/autogen/obspy.core.event.read_events.html#obspy.core.event.read_events

#in_file = '../Catalog/test_puerto_rico_catalog.txt'
#
##unpick = Unpickler()
##cat = unpick.load(file=in_file)
##print(cat)
#
## Read all events
#cat = read_events(in_file, format='QUAKEML')
#print(cat)


# Read events from QuakeML files named with event ID
# This has catalog phase picks and focal mechanism/moment tensor
in_ev_files = '../Catalog/CatalogEvents/*.xml'
cat = read_events(in_ev_files, format='QUAKEML')
print(cat)

num_ev_focal = 0
for ev in cat:
   print("Number of picks: ", len(ev.picks))
   if (len(ev.focal_mechanisms) > 0):
      print(ev.short_str())
      num_ev_focal += 1
print("Number of events with focal mechanism data: ", num_ev_focal)
