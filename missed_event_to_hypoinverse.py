import datetime
import json
from obspy import read_events

# Missed event analysis - output HYPOINVERSE arc file for consistent processing
# Must run compare_catalog_eqtransformer_events.py before running this script

# Inputs
#in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
#in_missed_event_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/events_MISSED_eqt_20200107_20200114.txt'
#in_missed_event_xml_dir = '../LargeAreaEQTransformer/CatalogEvents/'
#out_missed_event_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/events_MISSED_eqt_evid_20200107_20200114.txt'
#out_hinv_arc_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/cat_real_magmiss_locate_pr.sum'

#in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/station_list_edited_20180101_20201101.json'
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/events_MISSED_eqt_2018_2020.txt'
#in_missed_event_xml_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/CatalogEvents/'
#out_missed_event_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/events_MISSED_eqt_evid_2018_2020.txt'
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/cat_real_magmiss_locate_pr.sum'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_missed_event_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/CatalogEventsDownload20210601/'
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/events_MISSED_EQT_20200107_20200114.txt'
#out_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/events_MISSED_EQT_EVID_20200107_20200114.txt'
#out_missed_us_event_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_EQT_EVID_20200107_20200114.txt' # incomplete but that's ok
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/cat_real_magmiss_locate_pr.sum'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_missed_event_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/CatalogEventsDownload20210601/'
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/events_MISSED_EQT_20200107_20200114.txt'
#out_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/events_MISSED_EQT_EVID_20200107_20200114.txt'
#out_missed_us_event_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_EQT_EVID_20200107_20200114.txt' # incomplete but that's ok
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/cat_real_magmiss_locate_pr.sum'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_missed_event_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/CatalogEventsDownload20210608/'
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/events_MISSED_EQT_20191228_20200114.txt'
#out_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/events_MISSED_EQT_EVID_20191228_20200114.txt'
#out_missed_us_event_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_EQT_EVID_20191228_20200114.txt' # incomplete but that's ok
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/cat_real_magmiss_locate_pr.sum'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_missed_event_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/CatalogEventsDownload20210608/'
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/events_MISSED_EQT_20191228_20200114.txt'
#out_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/events_MISSED_EQT_EVID_20191228_20200114.txt'
#out_missed_us_event_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_EQT_EVID_20191228_20200114.txt' # incomplete but that's ok
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/cat_real_magmiss_locate_pr.sum'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_missed_event_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/CatalogEventsDownload20210610/'
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/events_MISSED_EQT_20180101_20210601.txt'
#out_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/events_MISSED_EQT_EVID_20180101_20210601.txt'
#out_missed_us_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_EQT_EVID_20180101_20210601.txt' # incomplete but that's ok
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/cat_real_magmiss_locate_pr.sum'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_missed_event_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/CatalogEventsDownload20210610/'
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_MISSED_EQT_20180101_20210601.txt'
#out_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_MISSED_EQT_EVID_20180101_20210601.txt'
#out_missed_us_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_EQT_EVID_20180101_20210601.txt' # incomplete but that's ok
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/cat_real_magmiss_locate_pr.sum'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/station_list_edited.json'
#in_missed_event_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/CatalogEventsDownload20211020/'
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_MISSED_EQT_20180101_20211001.txt'
#out_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_MISSED_EQT_EVID_20180101_20211001.txt'
#out_missed_us_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_EQT_EVID_20180101_20211001.txt' # incomplete but that's ok
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/cat_real_magmiss_locate_pr.sum'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
#in_missed_event_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/CatalogEventsDownload20220111/'
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MISSED_magcat_EQT_20180101_20220101.txt'
#out_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MISSED_magcat_EQT_EVID_20180101_20220101.txt'
#out_missed_us_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/us_events_MISSED_magcat_EQT_EVID_20180101_20220101.txt' # incomplete but that's ok
#out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/cat_real_magmiss_locate_pr.arc'
#out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/cat_real_magmiss_locate_pr.sum'

in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
in_missed_event_xml_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/CatalogEventsDownload20220111/'
in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MISSED_magcat_EQT_20180101_20220101.txt'
out_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MISSED_magcat_EQT_EVID_20180101_20220101.txt'
out_missed_us_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/us_events_MISSED_magcat_EQT_EVID_20180101_20220101.txt' # incomplete but that's ok
out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/cat_real_magmiss_locate_pr.arc'
out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/cat_real_magmiss_locate_pr.sum'


stations_ = json.load(open(in_station_file))

# Loop over events in missed event file
nmissed = 0
nmissed_proc = 0
nmissed_us_proc = 0
missed_start_evid = 4000000 # arbitrary, but do not want overlap with existing event ID
fout_miss = open(out_missed_event_file, 'w')
fout_us_miss = open(out_missed_us_event_file, 'w')
fout_arc = open(out_hinv_arc_file, 'w')
fout_sum = open(out_hinv_sum_file, 'w')
with open(in_missed_event_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      origin_time = datetime.datetime.strptime(split_line[0], '%Y-%m-%dT%H:%M:%S.%f')
      origin_time_second = round(100*(origin_time.second + 1e-6*origin_time.microsecond))
      cat_lat = split_line[1]
      cat_lon = split_line[2]
      cat_depth = split_line[3]
      cat_mag = split_line[4]
      cat_evid = split_line[5]
      nmissed += 1
      print(origin_time, cat_lat, cat_lon, cat_depth, cat_mag, cat_evid)

      # Read in event file from catalog xml file, includes phase pick info
      in_xml_file = in_missed_event_xml_dir+cat_evid+'.xml'
      try:
         curr_ev = read_events(in_xml_file)
      except Exception:
         print("ERROR: Cannot find event xml file ", in_xml_file, ", skipping...", nmissed)
         continue

      # Assign new numeric event ID for this missed event
      missed_evid = missed_start_evid + nmissed

      # Output event info in HYPOINVERSE arc/sum files for missed events
      lat = cat_lat
      lat_int = int(float(lat))
      dlat = abs(float(lat) - float(lat_int))
      lat_min = str(format(round(100*float(dlat*60.0)), '04d'))
      if (lat_int < 0):
         lat_char = 'S'
      else:
         lat_char = ' '
      lat_int = abs(lat_int)
      lon = cat_lon
      lon_int = int(float(lon))
      dlon = abs(float(lon) - float(lon_int))
      lon_min = str(format(round(100*float(dlon*60.0)), '04d'))
      if (lon_int < 0):
         lon_char = 'W'
      else:
         lon_char = 'E'
      lon_int = abs(lon_int)
      depth_out = round(100*float(cat_depth))
      mag_out = round(100*float(cat_mag))
      fout_arc.write(('%4d%02d%02d%02d%02d%04d%2d%1s%4s%3d%1s%4s%5d                                                                                                    %10d %3d\n') % (origin_time.year, origin_time.month, origin_time.day, origin_time.hour, origin_time.minute, origin_time_second, lat_int, lat_char, lat_min, lon_int, lon_char, lon_min, depth_out, missed_evid, mag_out))
      fout_sum.write(('%4d%02d%02d%02d%02d%04d%2d%1s%4s%3d%1s%4s%5d                                                                                                    %10d %3d\n') % (origin_time.year, origin_time.month, origin_time.day, origin_time.hour, origin_time.minute, origin_time_second, lat_int, lat_char, lat_min, lon_int, lon_char, lon_min, depth_out, missed_evid, mag_out))

      # Get phase picks
      phase_list = curr_ev[0].picks
      npicks_proc = 0
      for pick in phase_list:
         sta = pick.waveform_id.station_code
         if (sta in stations_): # only output the phase if it is in our station list (json)
            net = pick.waveform_id.network_code
            chcode = pick.waveform_id.channel_code
            ph = pick.phase_hint
            arr_time = pick.time
            if (pick.onset == 'impulsive'):
               pick_type = 'I'
            else:
               pick_type = 'E'
            print(net, sta, chcode, ph, arr_time)

            # Possible phase names in catalog
            if ((ph == 'Pg') or (chcode[-1] == 'Z')):
               ph = 'P'
            if ((ph == 'Sg') or (chcode[-1] == 'E') or (chcode[-1] == 'N') or (chcode[-1] == '1') or (chcode[-1] == '2')):
               ph = 'S'

            # Phase specific output
            if (ph == 'P'):
               chan = 'HHZ'
               p_remark = pick_type+'P'
               s_remark = '  '
               p_res = '    '
               s_res = '    '
               p_weight = 0 # EQTransformer probability not carried over, assume best quality for now
               s_weight = 0
               p_arr_time_sec = round(100*(arr_time.second + 1e-6*arr_time.microsecond))
               s_arr_time_sec = 0.00
            elif (ph == 'S'):
               chan = 'HHE'
               p_remark = '  '
               s_remark = pick_type+'S'
               p_res = '    '
               s_res = '    '
               p_weight = 0
               s_weight = 0 # EQTransformer probability not carried over, assume best quality for now
               p_arr_time_sec = 0.00
               s_arr_time_sec = round(100*(arr_time.second + 1e-6*arr_time.microsecond))
            else: # ph == something else (not P or S)
               print("Not using this phase: ", ph, ", skipping...", chcode)
               continue

            # Output phase pick info in HYPOINVERSE arc files for missed events
            npicks_proc += 1
            fout_arc.write(('%-5s%2s  %3s %2s %1d%4d%02d%02d%02d%02d%5d%4s   %5d%2s %1d%4s\n') % (sta, net, chan, p_remark, p_weight, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, p_arr_time_sec, p_res, s_arr_time_sec, s_remark, s_weight, s_res))

         else: # pick station not in our list, ignore
            print("PICK AT THIS STATION NOT USED: ", sta)
      print("Number of phase picks processed in our station list: ", npicks_proc, "/", len(phase_list), "\n")

      # Output missed event file
      nmissed_proc += 1
      fout_miss.write(('%s %s\n') % (line.replace('\n',''), missed_evid))

      if (cat_evid[0:2] == 'us'):
         nmissed_us_proc += 1
         fout_us_miss.write(('%s %s\n') % (line.replace('\n',''), missed_evid))

      # Output event ID line in HYPOINVERSE arc files for missed events
      fout_arc.write("{:<62}".format(' ')+"%10d"%(missed_evid)+'\n');

fout_miss.close()
fout_us_miss.close()
fout_arc.close()
fout_sum.close()
print("Number of missed events processed with xml event file: ", nmissed_proc, "/", nmissed)
print("Number of missed events with us: ", nmissed_us_proc)

