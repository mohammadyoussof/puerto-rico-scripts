from obspy import read
from obspy.core.stream import Stream
import datetime
import matplotlib.pyplot as plt
import matplotlib
import sys
import os
import json
import glob
from matplotlib import rcParams
import utils_hypoinverse as utils_hyp

matplotlib.use('Agg')
rcParams['pdf.fonttype'] = 42


if len(sys.argv) != 3:
   print("Usage: python PARTIALplot_event_waveform_gathers.py <start_ind> <end_ind>")
   sys.exit(1)

IND_FIRST = int(sys.argv[1])
IND_LAST = int(sys.argv[2])
print("PROCESSING:", IND_FIRST, IND_LAST)
   
# Inputs
###in_hinv_arc_file = '../LargeAreaEQTransformer/association/merged_locate_pr.arc' # use the merged file
##in_hinv_arc_file = '../LargeAreaEQTransformer/REAL/Events/real_locate_pr.arc' # use the merged file
##in_hinv_arc_file = '../LargeAreaEQTransformer/REAL/Events/merged_real_locate_pr.arc' # use the merged file
##in_hinv_arc_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/merged_real_magmiss_locate_pr.arc' # use the merged file
##in_hinv_arc_file = '../LargeAreaEQTransformer/PhaseLink/merged_pl_locate_pr.arc' # use the merged file
##in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
##in_mseed_dir = '../LargeAreaEQTransformer/downloads_mseeds/'
##in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_locate_pr.arc' # use the merged file
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/merged_real_locate_pr.arc' # use the merged file
##in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PhaseLink/merged_pl_locate_pr.arc' # use the merged file
#in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
##in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/merged_real_magmiss_locate_pr.arc' # use the merged file
##in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/station_list_edited_20180101_20201101.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/downloads_mseeds/'
##wtime_before = 30 # time window before origin time (s)
##wtime_after = 60 # time window after origin time (s)
##wtime_before = 15 # time window before origin time (s)
##wtime_after = 45 # time window after origin time (s)
#wtime_before = 15 # time window before origin time (s)
##wtime_after = 105 # time window after origin time (s)
#wtime_after = 75 # time window after origin time (s)

# Outputs
#out_dir = '../LargeAreaEQTransformer/WaveformPlots/'
#out_dir = '../LargeAreaEQTransformer/REALWaveformPlots/'
#out_dir = '/media/yoon/INT01/PuertoRico/LargeAreaEQTransformer/REALWaveformPlots/'
#out_dir = '/media/yoon/INT01/PuertoRico/LargeAreaEQTransformer/20200107_20200114_Model1/MissedREALWaveformPlots/'
#out_dir = '/media/yoon/INT01/PuertoRico/LargeAreaEQTransformer/PLWaveformPlots/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/WaveformPlots/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REALWaveformPlots/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/Model2REALWaveformPlots/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/PLWaveformPlots/'
#out_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/MissedREALWaveformPlots/'


#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/WaveformPlots/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/MissedWaveformPlots/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/WaveformPlots/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/MissedWaveformPlots/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/WaveformPlots/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/MissedWaveformPlots/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/WaveformPlots/'

#in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/downloads_mseeds/'
#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/merged_real_magmiss_locate_pr.arc' # use the merged file
#out_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/MissedWaveformPlots/'

in_station_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
in_mseed_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/downloads_mseeds/'
in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magcat_locate_pr.arc' # use the merged file
out_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/WaveformPlots/'



# Defaults
#wtime_before = 15 # time window before origin time (s)
#wtime_after = 75 # time window after origin time (s)

# Events close in time - verify waveforms visually
wtime_before = 0 # time window before origin time (s)
wtime_after = 40 # time window after origin time (s)
#wtime_after = 15 # time window after origin time (s)

if not os.path.exists(out_dir):
   os.makedirs(out_dir)
#out_width = 1200
#out_height = 1600

# Read in and store event and phase data from HYPOINVERSE arc file
[event_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)


print("\n ------------------- OUTPUT PLOTS --------------------------\n")
stations_ = json.load(open(in_station_file))
for kk in range(IND_FIRST, IND_LAST):
   curr_ev = event_dict[ev_id_list[kk]]

   # Get time window for plots
   ev_origin_time = curr_ev['event'][0]
   start_time = ev_origin_time - wtime_before
   end_time = ev_origin_time + wtime_after
   print(kk, ev_id_list[kk], ev_origin_time, start_time, end_time)

   # Read in time window for plots from daylong mseed files
   st_all = Stream()
   for sta,val in stations_.items():
      net = val['network']
      ev_origin_date = datetime.datetime(ev_origin_time.year, ev_origin_time.month, ev_origin_time.day)
      origin_date_str = datetime.datetime.strftime(ev_origin_date, '%Y%m%d')
      mseed_files = glob.glob(in_mseed_dir+'*/'+sta+'/'+net+'.'+sta+'*Z__'+origin_date_str+'T000000Z'+'__*') # vertical only
      if (len(mseed_files) > 0):
         st = read(in_mseed_dir+'*/'+sta+'/'+net+'.'+sta+'*Z__'+origin_date_str+'T000000Z'+'__*mseed') # vertical only
         st_slice = st.slice(start_time, end_time)

         # Filter the cut data
         st_slice.detrend(type='demean')
         st_slice.detrend(type='linear')
         st_slice.filter('bandpass', freqmin=2, freqmax=15, corners=2, zerophase=False)

         # Add to the entire stream of data
         st_all += st_slice

   # Plot waveform gathers with phase picks from HYPOINVERSE arc file
   if (len(st_all) > 0):
      print(st_all.__str__(extended=True))
      fplot, ax = plt.subplots(len(st_all), sharex=True, figsize=(16,42))

      for ist in range(0,len(st_all)):
         # Plot waveforms
         ax[ist].plot(st_all[ist].times(), st_all[ist].data, color='k', linewidth=0.5)
         ax[ist].set_title(st_all[ist].stats.network+'.'+st_all[ist].stats.station+'.'+st_all[ist].stats.channel, y=0.7)

         # Loop over phase picks and plot them
         for netsta,val in curr_ev['P'].items():
            curr_net = netsta[0:2]
            curr_sta = netsta[2:]
            if ((curr_net == st_all[ist].stats.network) and (curr_sta == st_all[ist].stats.station)):
               ph_UTC_time = val[1]
               ph_rel_time = ph_UTC_time - st_all[ist].stats.starttime
               ax[ist].axvline(x=ph_rel_time, color='b')

         for netsta,val in curr_ev['S'].items():
            curr_net = netsta[0:2]
            curr_sta = netsta[2:]
            if ((curr_net == st_all[ist].stats.network) and (curr_sta == st_all[ist].stats.station)):
               ph_UTC_time = val[1]
               ph_rel_time = ph_UTC_time - st_all[ist].stats.starttime
               ax[ist].axvline(x=ph_rel_time, color='r')

      plt.xlabel("Time (s)")
      plt.suptitle("starttime = "+st_all[ist].stats.starttime.strftime('%Y%m%dT%H%M%S.%f')+", origin time = "+ev_origin_time.strftime('%Y%m%dT%H%M%S.%f'), y=0.95)
      plt.tight_layout()
      output_plot_file = out_dir+'phaseplot_event'+format(ev_id_list[kk],'06d')+'_'+ev_origin_time.strftime('%Y%m%dT%H%M%S.%f')+'.png'
      plt.savefig(output_plot_file)
      plt.close(fplot)
   #   st_all.plot(fig=fig, equal_scale=False, size=(out_width,out_height), outfile=output_plot_file)

