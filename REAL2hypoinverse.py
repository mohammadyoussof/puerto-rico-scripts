import datetime
# Convert REAL phase output file to HYPOINVERSE phase input file

# from EQTransformer/utils/associator.py
def _weighcalculator_prob(pr):
    'calculate the picks weights'
    weight = 4
    if pr > 0.6:
        weight = 0
    elif pr <= 0.6 and pr > 0.5:
        weight = 1
    elif pr <= 0.5 and pr > 0.2:
        weight = 2
    elif pr <= 0.2 and pr > 0.1:
        weight = 3  
    elif pr <= 0.1:
        weight = 4 
    return weight


#in_real_phase_file = '../LargeAreaEQTransformer/REAL/Events/LargeAreaEQTransformer_phase_sel.txt'
#out_hinv_phase_file = '../LargeAreaEQTransformer/REAL/Events/LargeAreaEQTransformer.phs'
#in_real_phase_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/Events/FullEQTransformer_phase_sel.txt'
#out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/REAL/HYPOINVERSE/FullEQTransformer.phs'

#in_real_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/Events_VELPRSN/EQT_20200107_20200114_phase_sel.txt'
#out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/EQT_20200107_20200114.phs'
#in_real_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/Events_VELZHANG/EQT_20200107_20200114_phase_sel.txt'
#out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/EQT_20200107_20200114.phs'

#in_real_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/Events_VELPRSN/EQT_20191228_20200114_phase_sel.txt'
#out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/EQT_20191228_20200114.phs'
#in_real_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/Events_VELZHANG/EQT_20191228_20200114_phase_sel.txt'
#out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/EQT_20191228_20200114.phs'

#in_real_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/Events_VELPRSN/EQT_20180101_20210601_phase_sel.txt'
#out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/EQT_20180101_20210601.phs'
#in_real_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/Events_VELZHANG/EQT_20180101_20210601_phase_sel.txt'
#out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/EQT_20180101_20210601.phs'

#in_real_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/Events_VELPRSN/EQT_20180101_20211001_phase_sel.txt'
#out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/EQT_20180101_20211001.phs'
#in_real_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/Events_VELZHANG/EQT_20180101_20211001_phase_sel.txt'
#out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/EQT_20180101_20211001.phs'

#in_real_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/Events_VELPRSN/EQT_20180101_20220101_phase_sel.txt'
#out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/EQT_20180101_20220101.phs'
in_real_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/Events_VELZHANG/EQT_20180101_20220101_phase_sel.txt'
out_hinv_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/EQT_20180101_20220101.phs'

ev_id = 200001 # EQTransformer convention for event id
fout = open(out_hinv_phase_file, 'w')
with open(in_real_phase_file, 'r') as freal:
   for line in freal:
      split_line = line.split()
      if (split_line[0].isnumeric()): # event line
         year = int(split_line[1])
         month = int(split_line[2])
         day = int(split_line[3])
         hour = int(split_line[4][0:2])
         minute = int(split_line[4][3:5])
         origin_time_nosec = datetime.datetime(year, month, day, hour, minute)
         origin_delta = datetime.timedelta(seconds=float(split_line[4][6:12])) # might be negative
         origin_time = origin_time_nosec + origin_delta
         origin_time_second = round(100*(origin_time.second + 1e-6*origin_time.microsecond))
         lat = split_line[7]
         lat_int = int(float(lat))
         dlat = abs(float(lat) - float(lat_int))
         lat_min = str(format(round(100*float(dlat*60.0)), '04d'))
         if (lat_int < 0):
            lat_char = 'S'
         else:
            lat_char = ' '
         lat_int = abs(lat_int)
         lon = split_line[8]
         lon_int = int(float(lon))
         dlon = abs(float(lon) - float(lon_int))
         lon_min = str(format(round(100*float(dlon*60.0)), '04d'))
         if (lon_int < 0):
            lon_char = 'W'
         else:
            lon_char = 'E'
         lon_int = abs(lon_int)
         depth = split_line[9]
         depth_out = round(100*float(depth))
         nphases = int(split_line[14])
         count_phases = 0
         fout.write(('%4d%02d%02d%02d%02d%04d%2d%1s%4s%3d%1s%4s%5d%3d\n') % (origin_time.year, origin_time.month, origin_time.day, origin_time.hour, origin_time.minute, origin_time_second, lat_int, lat_char, lat_min, lon_int, lon_char, lon_min, depth_out, 0))
#         print(ev_id, year, month, day, hour, minute, second, lat, lat_int, lat_char, dlat, lat_min, lon, lon_int, lon_char, dlon, lon_min, depth_out, nphases, origin_time_str, origin_time)
         print(split_line)
      else: # phase line
         net = split_line[0]
         sta = "{:<5}".format(split_line[1])
         ph = split_line[2]
         tt = float(split_line[4])
         delta = datetime.timedelta(seconds=tt)
         arr_time = origin_time + delta
         res = float(split_line[6])
         weight = float(split_line[7])
         if (ph == 'P'):
            chan = 'HHZ'
            p_remark = 'IP'
            s_remark = '  '
            p_res = str(format(round(100*res), '4d'))
            s_res = '    '
            p_weight = _weighcalculator_prob(weight) 
            s_weight = 0
            p_arr_time_sec = round(100*(arr_time.second + 1e-6*arr_time.microsecond))
            s_arr_time_sec = 0.00
         else: # ph == 'S'
            chan = 'HHE'
            p_remark = '  '
            s_remark = 'ES'
            p_res = '    '
            s_res = str(format(round(100*res), '4d'))
            p_weight = 0
            s_weight = _weighcalculator_prob(weight) 
            p_arr_time_sec = 0.00
            s_arr_time_sec = round(100*(arr_time.second + 1e-6*arr_time.microsecond))
         count_phases += 1
         fout.write(('%5s%2s  %3s %2s %1d%4d%02d%02d%02d%02d%5d%4s   %5d%2s %1d%4s\n') % (sta, net, chan, p_remark, p_weight, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, p_arr_time_sec, p_res, s_arr_time_sec, s_remark, s_weight, s_res))
#         print(net, sta, ph, tt, arr_time, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, round(100*(arr_time.second + 1e-6*arr_time.microsecond)), res, p_res, s_res, weight, p_weight, s_weight)
#         print(split_line)
      if (count_phases == nphases):
         fout.write("{:<62}".format(' ')+"%10d"%(ev_id)+'\n');
         ev_id += 1
fout.close()



