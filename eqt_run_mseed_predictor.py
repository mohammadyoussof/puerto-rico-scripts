from EQTransformer.core.mseed_predictor import mseed_predictor
import sys

# eqt_run_mseed_predictor.py: script to run EQTransformer detector/picker on MiniSEED waveform data
# --> To run in parallel: called by eqt_parallel_run_mseed_predictor.py

if len(sys.argv) != 3:
   print("Usage: python eqt_run_mseed_predictor.py <in_mseed_dir> <out_det_dir>")
   print("Usage Example: python eqt_run_mseed_predictor.py /media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/2020-01-07/ /media/yoon/INT01/PuertoRico/EQT_20200107_20200114/mseed_detections/2020-01-07/")
   print("Exiting, not enough arguments...")
   sys.exit(1)

IN_MSEED_DIR = str(sys.argv[1])
OUT_DET_DIR = str(sys.argv[2])
print("PROCESSING:", IN_MSEED_DIR, OUT_DET_DIR)
   

#--------------------------START OF INPUTS------------------------
# Model parameters
model_path="/home/yoon/Documents/EQTransformer/ModelsAndSampleData/"
model_file='EqT_model.h5' # initial model - minimize false positives
#model_file='EqT_model2.h5' # new model - minimize false negatives

# Predictor parameters
over_lap = 0.75 # overlap fraction between adjacent time windows.  Do not use default value over_lap=0.3 -> missing many picks
#num_plots = 1000
num_plots = 1

# Station file (input)
#in_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list.json'
#in_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list.json'
in_station_json_file='/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list.json'
#--------------------------END OF INPUTS------------------------

# Run the predictor directly on MiniSEED data
mseed_predictor(input_dir=IN_MSEED_DIR, input_model=model_path+model_file, stations_json=in_station_json_file,
   output_dir=OUT_DET_DIR, detection_threshold=0.3, P_threshold=0.1, S_threshold=0.1,
   number_of_plots=num_plots, plot_mode='time_frequency', overlap=over_lap, batch_size=500)
