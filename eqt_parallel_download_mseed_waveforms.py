import datetime
from dateutil.relativedelta import relativedelta
import os
import time
import subprocess
import multiprocessing

# eqt_parallel_download_mseed_waveforms.py: script to parallelize download MiniSEED waveform data
# --> Calls eqt_download_mseed_waveforms.py

def get_list_of_days_mseed(day_start, day_end, out_base_mseed_dir, delta, folder_format):
   start_date = datetime.datetime.strptime(day_start, "%Y-%m-%d")
   end_date = datetime.datetime.strptime(day_end, "%Y-%m-%d")
   date_inputs = []
   # Loop over days
   t0 = time.time()
   while (start_date < end_date):
      # Create directory for start_date day if it doesn't already exist
      out_mseed_dir = out_base_mseed_dir+datetime.datetime.strftime(start_date, folder_format)+'/'
      if not os.path.exists(out_mseed_dir):
         os.makedirs(out_mseed_dir)
      next_date = start_date + delta
      date_str_start = datetime.datetime.strftime(start_date, "%Y-%m-%d")
      date_str_next = datetime.datetime.strftime(next_date, "%Y-%m-%d")
      date_inputs.append([date_str_start, date_str_next, out_mseed_dir])
      start_date = next_date
   return date_inputs 


def run_download_script(date_inputs):
   print("date_inputs = ", date_inputs)

   print('python eqt_download_mseed_waveforms.py %s %s %s' % (date_inputs[0], date_inputs[1], date_inputs[2]))
   process = subprocess.Popen(('python eqt_download_mseed_waveforms.py %s %s %s' % (date_inputs[0], date_inputs[1], date_inputs[2])), stdout=subprocess.PIPE, shell=True)

   output, error = process.communicate()
   print(output.decode('UTF-8').strip())



#--------------------------START OF INPUTS------------------------
## EQT_20200107_20200114
#day_start="2020-01-07"
#day_end="2020-01-14"
#out_base_mseed_dir = "/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/"
#delta = datetime.timedelta(days=1) # one sub-directory per day
#folder_format = "%Y-%m-%d"

## EQT_20191228_20200114
##day_start="2019-12-28"
##day_end="2020-01-03"
##day_start="2020-01-03"
##day_end="2020-01-09"
#day_start="2020-01-09"
#day_end="2020-01-14"
#out_base_mseed_dir = "/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/downloads_mseeds/"
#delta = datetime.timedelta(days=1) # one sub-directory per day
#folder_format = "%Y-%m-%d"

# EQT_20180101_20220101
day_start="2018-01-01"
#day_end="2018-07-01"
#day_start="2018-07-01"
#day_end="2019-01-01"
#day_start="2019-01-01"
#day_end="2019-07-01"
#day_start="2019-07-01"
#day_end="2020-01-01"
#day_start="2020-01-01"
#day_end="2020-07-01"
#day_start="2020-07-01"
#day_end="2021-01-01"
#day_start="2021-01-01"
#day_end="2021-05-01"
#day_start="2021-05-01"
#day_end="2021-06-01"
#day_start="2021-07-01"
#day_end="2021-08-01"
#day_start="2021-08-01"
#day_end="2021-09-01"
#day_start="2021-09-01"
#day_end="2021-10-01"
#day_start="2021-11-01"
#day_end="2021-12-01"
#day_start="2021-12-01"
day_end="2022-01-01"
out_base_mseed_dir = "/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/downloads_mseeds/"
delta = relativedelta(months=1) # one sub-directory per month
folder_format = "%Y_%m"
#--------------------------END OF INPUTS------------------------

if not os.path.exists(out_base_mseed_dir):
   os.makedirs(out_base_mseed_dir)

# Get input parameters for each parallel run: one folder per day
date_inputs = get_list_of_days_mseed(day_start, day_end, out_base_mseed_dir, delta, folder_format)
print("len(date_inputs) = ", len(date_inputs))
print("date_inputs = ", date_inputs)

#num_cores = multiprocessing.cpu_count()
num_cores = 6 # More than this -> IRIS Client time out
print("num_cores = ", num_cores)
# Run download script in parallel: eqt_download_mseed_waveforms.py
pool = multiprocessing.Pool(processes=num_cores)
pool.map(run_download_script, date_inputs)

