#!/bin/bash

# Plot Puerto Rico earthquakes on a map

#gmt gmtset BASEMAP_TYPE plain
#gmt gmtset HEADER_FONT_SIZE 24p
#gmt gmtset LABEL_FONT_SIZE 16p # smaller font for labels
##gmt gmtset PLOT_DEGREE_FORMAT +DF
#gmt gmtset PLOT_DEGREE_FORMAT -DF
#gmt gmtset GRID_PEN_PRIMARY 0.1p
#
gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

gmt gmtset LABEL_FONT_SIZE 20p
gmt gmtset FONT_ANNOT 20p
#gmt gmtset LABEL_FONT_SIZE 32p #poster
#gmt gmtset FONT_ANNOT 32p #poster

out_color_filename=depthcolors.cpt
out_color_file=../EQT_20180101_20220101/Plots/${out_color_filename}
out_color_time_filename=timecolors.cpt
out_color_time_file=../EQT_20180101_20220101/Plots/${out_color_time_filename}

#in_cat_file=../Catalog/catalog_new_puerto_rico_20200107_20200114.txt
#out_map_cat_depth_file=../Plots/puerto_rico_depth_20200107_20200114_small_map
#out_map_cat_time_file=../Plots/puerto_rico_time_20200107_20200114_small_map
####in_eqt_hinv_file=../Catalog/TestEQTransformer_HYPOINVERSE_puerto_rico_catalog_20200107_20200114.txt
####out_map_eqt_hinv_depth_file=../Plots/puerto_rico_eqt_20200107_20200114_small_map
#####in_sta_file=../Catalog/pr_stations.txt
####in_cat_file=../Catalog/catalog_new_puerto_rico_2018_2020.txt
####out_map_cat_file=../Plots/puerto_rico_2018_2020_small_map
#in_cat_file=../Catalog/catalog_new_puerto_rico_2018_2020.txt
#in_cat_file=../Catalog/catalog_new_puerto_rico_2018_2020_diam.txt
#out_map_cat_depth_file=../Plots/catalog_new_puerto_rico_2018_2020_depth
#out_map_cat_time_file=../Plots/catalog_new_puerto_rico_2018_2020_time

##in_cat_file=../Catalog/EQT_20180101_20211001/test_catalog.txt
#in_cat_file=../Catalog/EQT_20180101_20211001/catalog_new_puerto_rico_20180101_20211001_download20211020_diam.txt
##out_map_cat_depth_file=../EQT_20180101_20211001/Plots/catalog_new_puerto_rico_20180101_20191228_depth
##out_map_cat_time_file=../EQT_20180101_20211001/Plots/catalog_new_puerto_rico_20180101_20191228_time
#out_map_cat_depth_file=../EQT_20180101_20211001/Plots/catalog_new_puerto_rico_20180101_20211001_depth
#out_map_cat_time_file=../EQT_20180101_20211001/Plots/catalog_new_puerto_rico_20180101_20211001_time

in_cat_file=../Catalog/EQT_20180101_20220101/catalog_new_puerto_rico_20180101_20220101_download20220111_diam.txt
out_map_cat_depth_file=../EQT_20180101_20220101/Plots/catalog_new_puerto_rico_20180101_20220101_depth
out_map_cat_time_file=../EQT_20180101_20220101/Plots/catalog_new_puerto_rico_20180101_20220101_time

#in_mt_file=../Catalog/SLU_MT_2018_2020_puerto_rico_times.txt
#out_map_mt_file=../Plots/SLU_MT_2018_2020_puerto_rico

#in_sta_file=../Catalog/FullEQTransformer_pr_stations_large.txt
in_temp_sta_file=../Catalog/FullEQTransformer_pr_stations_large_temporary.txt
in_sta_file=../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_pr_stations.txt

#min_lat=17
#max_lat=19
#min_lon=-68
#max_lon=-65
#
#min_lat=17.5
#max_lat=18.5
#min_lon=-67.5
#max_lon=-66.0

min_lat=17.6
max_lat=18.3
min_lon=-67.3
max_lon=-66.4

reg=-R${min_lon}/${max_lon}/${min_lat}/${max_lat}
proj=-JM16
echo ${reg}
lat_lon_spacing=0.3
#lat_lon_spacing=0.1

region_inset=-R-75/-62/15/22
projection_inset=-JM2.0i

# Cross section parameters
strike1_angle=15
strike1_length=30
strike1_proj_width=10
strike1_center_lat=17.9
strike1_center_lon=-66.85
strike1_bproj=-JX30/15
strike1_brange=-R-30/30/-30/0

strike2_angle=95
strike2_length=40
strike2_proj_width=10
strike2_center_lat=17.93
strike2_center_lon=-66.85
strike2_bproj=-JX40/15
strike2_brange=-R-40/40/-30/0


# Time slice ranges
FIRST_DAY=0 # 2018-01-01
#LAST_DAY=726 # 2019-12-28
#LAST_DAY=1369 # 2021-10-01
#LAST_DAY=1369 # 2021-10-01
LAST_DAY=1461 # 2022-01-01


# Entire Model1 time range
#FIRST_DAY=0 # 2018-01-01
#LAST_DAY=1035 # 2020-11-01

# 0. Before start of sequence
#FIRST_DAY=0 # 2018-01-01
#LAST_DAY=726 # 2019-12-28

# 1. Between start of sequence and mainshock
#FIRST_DAY=726 # 2019-12-28
#LAST_DAY=736 # 2020-01-07

# First week after mainshock
#FIRST_DAY=736 # 2020-01-07
#LAST_DAY=743 # 2020-01-14

# First day after mainshock
#FIRST_DAY=736 # 2020-01-07
#LAST_DAY=737 # 2020-01-08

# Day 1-4 after mainshock
#FIRST_DAY=737 # 2020-01-08
#LAST_DAY=740 # 2020-01-11

# Day 5-7 after mainshock
#FIRST_DAY=740 # 2020-01-11
#LAST_DAY=743 # 2020-01-14

# 2. Day 0-3 after mainshock
#FIRST_DAY=736 # 2020-01-07
#LAST_DAY=739 # 2020-01-10

# 3. Day 3-7 after mainshock
#FIRST_DAY=739 # 2020-01-10
#LAST_DAY=743 # 2020-01-14

# 4. Day 7-25 after mainshock
#FIRST_DAY=743 # 2020-01-14
#LAST_DAY=761 # 2020-02-01

# Day 7-14 after mainshock
#FIRST_DAY=743 # 2020-01-14
#LAST_DAY=750 # 2020-01-21

# Day 14-25 after mainshock - to end of 2020-01
#FIRST_DAY=750 # 2020-01-21
#LAST_DAY=761 # 2020-02-01

# 5. 2020-02
#FIRST_DAY=761 # 2020-02-01
#LAST_DAY=790 # 2020-03-01

# 6. 2020-03 to 2020-05
#FIRST_DAY=790 # 2020-03-01
#LAST_DAY=851 # 2020-05-01

# 2020-03
#FIRST_DAY=790 # 2020-03-01
#LAST_DAY=821 # 2020-04-01

# 2020-04
#FIRST_DAY=821 # 2020-04-01
#LAST_DAY=851 # 2020-05-01

# 7. 2020-05 to 2020-06-13
#FIRST_DAY=851 # 2020-05-01
#LAST_DAY=894 # 2020-06-13

# 8. 2020-06-13 to 2020-09-01
#FIRST_DAY=894 # 2020-06-13
#LAST_DAY=974 # 2020-09-01

# 9. 2020-09-01 to 2020-11-01
#FIRST_DAY=974 # 2020-09-01
#LAST_DAY=1035 # 2020-11-01

# 2020-05
#FIRST_DAY=851 # 2020-05-01
#LAST_DAY=882 # 2020-06-01

# 2020-06
#FIRST_DAY=882 # 2020-06-01
#LAST_DAY=912 # 2020-07-01

# 2020-07
#FIRST_DAY=912 # 2020-07-01
#LAST_DAY=943 # 2020-08-01

# 2020-08
#FIRST_DAY=943 # 2020-08-01
#LAST_DAY=974 # 2020-09-01

# 2020-09
#FIRST_DAY=974 # 2020-09-01
#LAST_DAY=1004 # 2020-10-01

# 2020-10
#FIRST_DAY=1004 # 2020-10-01
#LAST_DAY=1035 # 2020-11-01



##### COLOR BY DEPTH #####

gmt makecpt -Cinferno -I -T0/20/0.01 > ${out_color_file}

#gmt begin ${out_map_cat_depth_file}
gmt begin ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}f0.1g -BneWS #-U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G210 -Slightskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm
#   awk '{print $2, $3, $4, $5*$5*0.01 + 0.0200}' ${in_cat_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $5*$5*0.01 + 0.0200}' ${in_cat_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $7/6.26172}' ${in_cat_file} | gmt plot ${proj} ${reg} -Scc -W0.01c -G255 -BneWS -: -C${out_color_file}
#   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $7/6.26172}' | gmt plot ${proj} ${reg} -Scc -W0.01c -G255 -BneWS -: -C${out_color_file}
##   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Scc -W0.01c -G255 -BneWS -: -C${out_color_file}
   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.3+cl -BneWS -: -C${out_color_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Gwhite -:
   awk '{print $2 " " $3 " " $4}' ${in_temp_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,blue -Gyellow -:
#   gmt colorbar -C${out_color_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Depth (km)"

   # Project seismicity along one direction
#   awk '{print $3, $2, $4, $6, $5, $1}' ${in_cat_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_cat_depth_file}_seismicity_time_projection${strike1_angle}.txt # plot all events
#   awk '{print $3, $2, $4, $6, $5, $1}' ${in_cat_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_cat_depth_file}_seismicity_time_projection${strike2_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1}' ${in_cat_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1}' ${in_cat_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1, $7}' ${in_cat_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1, $7}' ${in_cat_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt # plot all events

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

#   # Add inset
#   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
#   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
#   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
#${min_lon} ${min_lat}
#${min_lon} ${max_lat}
#${max_lon} ${max_lat}
#${max_lon} ${min_lat}
#${min_lon} ${min_lat}
#EOF
gmt end show


##### SLU MOMENT TENSORS #####

#gmt begin ${out_map_mt_file}_${FIRST_DAY}_${LAST_DAY}
#   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}f0.1g -BneWS #-U
#   gmt coast ${proj} ${reg} -W0.25p,black -Na -G200 -Sdeepskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($12/86400.) >= FD && ($12/86400.) <= LD) print $3,$2,$4,$6,$7,$8,$5,0,0}' ${in_mt_file} | gmt meca ${proj} ${reg} -V -Sa0.576
#   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Gwhite -:
#   awk '{print $2 " " $3 " " $4}' ${in_temp_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,blue -Gyellow -:
#
#   # Projection lines
#   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
#   gmt text ${proj} ${reg} -F+f24 << EOF
#-66.82 18.15 A'
#-66.95 17.65 A
#EOF
#   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
#   gmt text ${proj} ${reg} -F+f24 << EOF
#-67.25 17.95 B
#-66.45 17.90 B'
#EOF
#gmt end show


##### COLOR BY TIME #####

#gmt makecpt -Cviridis -I -T0/7/0.01 > ${out_color_time_file} # LargeAreaEQTransformer - 7 days
#start_date=2020-01-07
#gmt makecpt -Cviridis -I -T0/1035/0.01 > ${out_color_time_file} # FullEQTransformer - 1035 days
#gmt makecpt -Cviridis -I -T699/1035/0.01 > ${out_color_time_file} # FullEQTransformer - 699-1035 days (2019-12-01 to 2020-11-01)
#gmt makecpt -Crainbow -I -T0/726/0.01 > ${out_color_time_file} # FullEQTransformer - 0-726 days (2018-01-01 to 2019-12-28)
#gmt makecpt -Cviridis -I -T726/1035/0.01 > ${out_color_time_file} # FullEQTransformer - 726-1035 days (2019-12-28 to 2020-11-01)
#gmt makecpt -Cviridis -I -T726/1369/0.01 > ${out_color_time_file} # FullEQTransformer - 726-1369 days (2019-12-28 to 2021-10-01)
gmt makecpt -Cviridis -I -T726/1461/0.01 > ${out_color_time_file} # FullEQTransformer - 726-1461 days (2019-12-28 to 2022-01-01)
start_date=2018-01-01

#gmt begin ${out_map_cat_time_file}
gmt begin ${out_map_cat_time_file}_${FIRST_DAY}_${LAST_DAY}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}f0.1g -BneWS #-U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G210 -Slightskyblue -Lg-66.6/17.68+c-66.5/17.68+w20+f+lkm
#   awk '{print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_cat_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
   #awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $5*$5*0.01 + 0.0200}' ${in_cat_file} | gmt plot ${proj} ${reg} -Sc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $7/6.26172}' ${in_cat_file} | gmt plot ${proj} ${reg} -Scc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
#   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Scc -W0.01c -G255 -BneWS -: -C${out_color_time_file}
   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.3+cl -BneWS -: -C${out_color_time_file}
   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,black -Gwhite -:
   awk '{print $2 " " $3 " " $4}' ${in_temp_sta_file} | gmt plot ${proj} ${reg} -Si0.4 -W0.1c,blue -Gyellow -:
#   gmt colorbar -C${out_color_time_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Days since "${start_date}

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-66.82 18.15 A'
-66.95 17.65 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W2,0/0/0,- -Gblack
   gmt text ${proj} ${reg} -F+f24 << EOF
-67.25 17.95 B
-66.45 17.90 B'
EOF

#   # Add inset
#   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
#   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
#   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
#${min_lon} ${min_lat}
#${min_lon} ${max_lat}
#${max_lon} ${max_lat}
#${max_lon} ${min_lat}
#${min_lon} ${min_lat}
#EOF
gmt end show

gmt gmtset LABEL_FONT_SIZE 32p
gmt gmtset FONT_ANNOT 32p
#gmt gmtset LABEL_FONT_SIZE 56p #poster
#gmt gmtset FONT_ANNOT 56p #poster

#gmt begin ${out_map_cat_time_file}_seismicity_time_projection${strike1_angle}
gmt begin ${out_map_cat_time_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}
   gmt basemap ${strike1_bproj} ${strike1_brange} -Ba10f5 -BneWS+g210 #-U
#   awk '{print $4, $3*(-1.0), $10/86400., $9*$9*0.01 + 0.0200}' ${out_map_cat_depth_file}_seismicity_time_projection${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $4, $3*(-1.0), $10/86400., $9*$9*0.01 + 0.0200}' ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $4, $3*(-1.0), $10/86400., $11}' ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Length along cross-section A-A' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   sort -nk10,10 ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $4, $3*(-1.0), $10/86400., $11}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
   sort -nk10,10 ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike1_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $4, $3*(-1.0), $10/86400., $11}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
#   gmt colorbar -C${out_color_time_file} -Dx11.9i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

#gmt begin ${out_map_cat_time_file}_seismicity_time_projection${strike2_angle}
gmt begin ${out_map_cat_time_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}
   gmt basemap ${strike2_bproj} ${strike2_brange} -Ba10f5 -BneWS+g210 #-U
#   awk '{print $4, $3*(-1.0), $10/86400., $9*$9*0.01 + 0.0200}' ${out_map_cat_depth_file}_seismicity_time_projection${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $4, $3*(-1.0), $10/86400., $9*$9*0.01 + 0.0200}' ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $4, $3*(-1.0), $10/86400., $11}' ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa4+l"Length along cross-section B-B' (km)" -Bya2+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   sort -nk10,10 ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $4, $3*(-1.0), $10/86400., $11}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_time_file}
#   sort -nk10,10 ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $4, $3*(-1.0), $10/86400., $11}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
   sort -nk10,10 ${out_map_cat_depth_file}_${FIRST_DAY}_${LAST_DAY}_seismicity_time_projection${strike2_angle}.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($10/86400.) >= FD && ($10/86400.) <= LD) print $4, $3*(-1.0), $10/86400., $11}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.2+cl -C${out_color_time_file}
#   gmt colorbar -C${out_color_time_file} -Dx15.8i/3.0i/2.5i/0.15i -Bxaf+l"Days since "${start_date}
gmt end show

