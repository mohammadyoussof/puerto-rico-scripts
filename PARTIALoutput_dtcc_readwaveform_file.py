import os
import sys
import time
from obspy import read
#from obspy.signal.cross_correlation import xcorr_pick_correction
from cross_correlation_mod import xcorr_pick_correction
from utils_distance import distance
import utils_hypoinverse as utils_hyp


# diff_pick_travel_time_xcorr: compute differential travel time between event pair i and j with cross-correlation
# Inputs:
# t0_origin_i   Origin time for event i (UTCDateTime)
# pick_i        Initial arrival time for event i (s)
# data_i        Waveform data for window i (Trace)
# t0_origin_j   Origin time for event j (UTCDateTime)
# pick_j        Initial arrival time for event j (s)
# data_j        Waveform data for window j (Trace)
# t_before      Time before arrival time (P or S) pick for xcorr window selection (s)
# t_after       Time after arrival time (P or S) pick for xcorr window selection (s)
# cc_maxlag     Maximum lag time for cross correlation (s)
#
# Outputs:
# diff_tt       Differential travel time for event pair (s)
# diff_cc       Max cross correlation value (s)
#
def diff_pick_travel_time_xcorr(t0_origin_i, pick_i, data_i, t0_origin_j, pick_j, data_j, t_before, t_after, cc_maxlag, str_sta, str_comp, str_pick, str_i, str_j, out_plot_dir, flag_outplot=False):

   # Compute travel times
   tt_i = pick_i - t0_origin_i # Travel time for event i (UTCDateTime)
   tt_j = pick_j - t0_origin_j # Travel time for event j (UTCDateTime)

   # Compute correction to event j arrival time (s) that results in max CC within window
   diff_tt = 0.0
   diff_cc = 0.0
   out_pair_file = out_plot_dir+"ccplot_"+str_i+"_"+str_j+"_"+str_sta+"_"+str_comp+"_"+str_pick+"_"+str(tt_i)+"_"+str(tt_j)+".png"
   try:
      [pick_corr_j, diff_cc] = xcorr_pick_correction(pick_i, data_i, pick_j, data_j, t_before, t_after, cc_maxlag, plot=flag_outplot, filename=out_pair_file)
   except Exception as ex:
#      print(ex, "EXCEPTION xcorr_pick_correction")
      return [diff_tt, diff_cc, out_pair_file]  # return 0 for CC so will be ignored by caller

   diff_tt = tt_i - (tt_j + pick_corr_j) # Differential travel time (s) - HYPODD convention
#   print("Pick correction (s) =", pick_corr_j, ", Differential travel time (s) =", diff_tt, ", CC =", diff_cc)
   return [diff_tt, diff_cc, out_pair_file]


# add_diff_tt_cc_phase: add differential travel time and cross-correlation to the list
# Inputs:
# difftt, diffcc, ph_str, min_CC, max_CC, sta, flag_plot, out_pair_file, out_plot_dtcc_high_dir
# Outputs (to be updated from inputs):
# diff_sta, diff_tt, diff_cc, diff_ph, count
def add_diff_tt_cc_phase(difftt, diffcc, ph_str, min_CC, max_CC, sta, flag_plot, out_pair_file, out_plot_dtcc_high_dir, diff_sta, diff_tt, diff_cc, diff_ph, count):
   if ((diffcc >= min_CC) and (diffcc <= max_CC)):
      diff_sta.append(sta)
      diff_tt.append(difftt)
      diff_cc.append(diffcc)
      diff_ph.append(ph_str)
      if (flag_plot): # move plot to a different directory
         os.system("mv "+out_pair_file+" "+out_plot_dtcc_high_dir)
      return (count+1)
   else:
      return count


# get_waveform_pair: retrieve waveform pair for events i, j at a given station and component, to use for cross-correlation
# Inputs:
# i: index of first event in pair
# j: index of second event in pair
# sta: station string for both waveforms
# comp_str: component string for both waveforms: 'E', 'N', or 'Z'
# wvf: event waveform storage object
# Outputs:
# tr_i, tr_j: waveform pair for events i, j, as ObsPy Trace objects
###def get_waveform_pair(i, j, sta, comp_str, wvf):
###   wvf_i = wvf[i].select(station=sta, component=comp_str)
###   wvf_j = wvf[j].select(station=sta, component=comp_str)
def get_waveform_pair(i, j, sta, comp_str, st_i, st_j):
   wvf_i = st_i.select(station=sta, component=comp_str)
   wvf_j = st_j.select(station=sta, component=comp_str)
#   print("wvf_i = ", wvf_i, ", wvf_j = ", wvf_j)
   if (len(wvf_i) and len(wvf_j)): # check that data is available for both channels
      tr_i = wvf_i[0]
      tr_j = wvf_j[0]
#      print(tr_i)
#      print(tr_j)
      return [tr_i, tr_j]
   else:
      return [None, None]


def output_dtcc(in_hinv_arc_file, in_event_file_dir, out_dtcc_file, out_plot_dtcc_dir, out_plot_dtcc_high_dir,
      IND_I_FIRST, IND_I_LAST, IND_J_FIRST, IND_J_LAST):

   ########################## Parameters for cross correlation ########################## 
   # Windows for cross-correlating pairs of P,S phases (seconds) - adjust these after viewing plots
   P_t_before = 0.4
   P_t_after = 1.2
   P_cc_maxlag = 0.3
   print("P_t_before:", P_t_before, ", P_t_after:", P_t_after, ", P_cc_maxlag:", P_cc_maxlag)

   #S_t_before = 0.8
   S_t_before = 1.2
   S_t_after = 3.0
   S_cc_maxlag = 0.6
   print("S_t_before:", S_t_before, ", S_t_after:", S_t_after, ", S_cc_maxlag:", S_cc_maxlag)


   # Bandpass filter frequencies (Hz) - adjust these after viewing plots
   #P_freq_min = 2
   #P_freq_max = 10
   #print("P_freq_min:", P_freq_min, ", P_freq_max:", P_freq_max)
   #
   #S_freq_min = 2
   #S_freq_max = 10
   #print("S_freq_min:", S_freq_min, ", S_freq_max:", S_freq_max)

   freq_min = 2
   freq_max = 10
   print("freq_min:", freq_min, ", freq_max:", freq_max)


   # Minimum CC threshold to include event pair in dt.cc file
   min_P_CC = 0.6
   max_P_CC = 1.0
   print("P minimum CC threshold:", min_P_CC, ", P maximum CC threshold:", max_P_CC)

   min_S_CC = 0.65
   max_S_CC = 1.0
   print("S minimum CC threshold:", min_S_CC, ", S maximum CC threshold:", max_S_CC)


   # Maximum distance (km) between event pairs for inclusion in dt.cc file
   #max_dist = 10.0
   max_dist = 20.0
   print("max_dist (km):", max_dist)


   # Flags for outputting plots of all cross-correlations (if True, takes longer to run)
   flag_plot = False
   #flag_plot = True

   # Initialize count variables
   count_pair = 0
   count_P = 0
   count_S = 0
   ########################## Parameters for cross correlation ########################## 


   # Get catalog phase data from HYPOINVERSE arc file
   [event_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)

###   [event_orig_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)
###   event_dict = {}
###
###   # Read event waveforms from sac files
###   # This is memory intensive, but saves I/O read time later on
###   t0 = time.time()
###   wvf = {} # store all waveforms here: wvf[ievent] is a Stream object, stored in order of ev_id_list
###   for iev in range(IND_I_FIRST, IND_I_LAST):
###      event_dict[ev_id_list[iev]] = event_orig_dict[ev_id_list[iev]]
###      ev_str = str(ev_id_list[iev])
###      event_dir = in_event_file_dir+ev_str+'/'
###      event_files = event_dir+'event'+ev_str+'*.sac'
###      st = read(event_files, debug_headers=True)
###      # Filter here instead of just before xcorr to save runtime
###      st.detrend(type='demean')
###      st.detrend(type='linear')
###      st.filter('bandpass', freqmin=freq_min, freqmax=freq_max, corners=2, zerophase=False)
###      wvf[iev] = st
###
###   if ((IND_J_FIRST != IND_I_FIRST) or (IND_J_LAST != IND_I_LAST)):
###      for iev in range(IND_J_FIRST, IND_J_LAST):
###         event_dict[ev_id_list[iev]] = event_orig_dict[ev_id_list[iev]]
###         ev_str = str(ev_id_list[iev])
###         event_dir = in_event_file_dir+ev_str+'/'
###         event_files = event_dir+'event'+ev_str+'*.sac'
###         st = read(event_files, debug_headers=True)
###         # Filter here instead of just before xcorr to save runtime
###         st.detrend(type='demean')
###         st.detrend(type='linear')
###         st.filter('bandpass', freqmin=freq_min, freqmax=freq_max, corners=2, zerophase=False)
###         wvf[iev] = st
###
###   t_read = time.time() - t0
###   del event_orig_dict # free memory
###   print("Runtime (s), read in all event waveforms from SAC files:", t_read)
###   print("len(wvf) = ", len(wvf))


   flag_diagonal = False
   if ((IND_I_FIRST == IND_J_FIRST) and (IND_I_LAST == IND_J_LAST)):
      flag_diagonal = True

   # Differential times between pairs of P, S phases by cross correlation
   # Go through loop in order of ev_id_list
   t = time.time()
   fcc = open(out_dtcc_file, 'w')
   for i in range(IND_I_FIRST, IND_I_LAST):
      print("*********************************************************** i = ", i, "*******************************************")
      out_id_i = ev_id_list[i]
      t0_origin_i = event_dict[out_id_i]['event'][0] # origin time (UTCDateTime), event i
      lon_i = event_dict[out_id_i]['event'][1]
      lat_i = event_dict[out_id_i]['event'][2]
      depth_i = event_dict[out_id_i]['event'][3]

      if (flag_diagonal):
         IND_J_START = i+1
      else:
         IND_J_START = IND_J_FIRST

      #NEW#
      event_dir_i = in_event_file_dir+str(out_id_i)+'/'
      event_files_i = event_dir_i+'event'+str(out_id_i)+'*.sac'
      st_i = read(event_files_i)
      # Filter here instead of just before xcorr to save runtime
      st_i.detrend(type='demean')
      st_i.detrend(type='linear')
      st_i.filter('bandpass', freqmin=freq_min, freqmax=freq_max, corners=2, zerophase=False)

      t_i = time.time()
      for j in range(IND_J_START, IND_J_LAST):
         out_id_j = ev_id_list[j]
         t0_origin_j = event_dict[out_id_j]['event'][0] # origin time (UTCDateTime), event j
         lon_j = event_dict[out_id_j]['event'][1]
         lat_j = event_dict[out_id_j]['event'][2]
         depth_j = event_dict[out_id_j]['event'][3]

#         if (0 == j%100): # print progress
#         print("---------------------- i = ", i, "j = ", j, "id event i = ", out_id_i, "id event j = ", out_id_j, "------------------------------------------------------")
   #         print("Origin time event i = ", t0_origin_i, "Origin time event j = ", t0_origin_j)
   #         print("id event i = ", out_id_i, "id event j = ", out_id_j)

         # Compute distance between events, is it less than the maximum?
         event_dist = distance(lon_i, lat_i, depth_i, lon_j, lat_j, depth_j)
   #      print("Distance between events (km) = ", event_dist)
         if (event_dist <= max_dist):
            diff_sta = []
            diff_tt = []
            diff_cc = []
            diff_ph = []
 
            #NEW#
            event_dir_j = in_event_file_dir+str(out_id_j)+'/'
            event_files_j = event_dir_j+'event'+str(out_id_j)+'*.sac'
            st_j = read(event_files_j)
            # Filter here instead of just before xcorr to save runtime
            st_j.detrend(type='demean')
            st_j.detrend(type='linear')
            st_j.filter('bandpass', freqmin=freq_min, freqmax=freq_max, corners=2, zerophase=False)

            # Find matching station/channels with P phase for event pair (i,j)
            for key in event_dict[out_id_i]['P']: # here key is 'netsta'
               if key in event_dict[out_id_j]['P']:
                  pickp_i = event_dict[out_id_i]['P'][key][1] # P phase arrival time (UTCDateTime), event i
                  pickp_j = event_dict[out_id_j]['P'][key][1] # P phase arrival time (UTCDateTime), event j
                  p_sta = key[2:]
   #               print("Station = ", p_sta, ", Pick P time event i = ", pickp_i, "Pick P time event j = ", pickp_j)
###                  [tr_i_p, tr_j_p] = get_waveform_pair(i, j, p_sta, 'Z', wvf)
                  [tr_i_p, tr_j_p] = get_waveform_pair(i, j, p_sta, 'Z', st_i, st_j)
                  if ((tr_i_p is not None) and (tr_j_p is not None)):
                     [difftt_p, diffcc_p, out_pair_file] = diff_pick_travel_time_xcorr(t0_origin_i, pickp_i, tr_i_p, t0_origin_j, pickp_j, tr_j_p, P_t_before, P_t_after, P_cc_maxlag, tr_i_p.stats.station, tr_i_p.stats.channel, 'P', str(out_id_i), str(out_id_j), out_plot_dtcc_dir, flag_plot)
                     count_P = add_diff_tt_cc_phase(difftt_p, diffcc_p, 'P', min_P_CC, max_P_CC, p_sta, flag_plot, out_pair_file, out_plot_dtcc_high_dir, diff_sta, diff_tt, diff_cc, diff_ph, count_P)

            # Find matching station/channels with S phase for event pair (i,j)
            for key in event_dict[out_id_i]['S']: # here key is 'netsta'
               if key in event_dict[out_id_j]['S']:
                  picks_i = event_dict[out_id_i]['S'][key][1] # S phase arrival time (UTCDateTime), event i
                  picks_j = event_dict[out_id_j]['S'][key][1] # S phase arrival time (UTCDateTime), event j
                  s_sta = key[2:]
   #               print("Station = ", s_sta, ", Pick S time event i = ", picks_i, "Pick S time event j = ", picks_j)
                  difftt_s = 0.0
                  diffcc_s = 0.0

                  # E component
###                  [tr_i, tr_j] = get_waveform_pair(i, j, s_sta, 'E', wvf)
                  [tr_i, tr_j] = get_waveform_pair(i, j, s_sta, 'E', st_i, st_j)
                  if ((tr_i is not None) and (tr_j is not None)):
                     [difftt_s, diffcc_s, out_pair_file] = diff_pick_travel_time_xcorr(t0_origin_i, picks_i, tr_i, t0_origin_j, picks_j, tr_j, S_t_before, S_t_after, S_cc_maxlag, tr_i.stats.station, tr_i.stats.channel, 'S', str(out_id_i), str(out_id_j), out_plot_dtcc_dir, flag_plot)

                  # N component
###                  [tr_i_n, tr_j_n] = get_waveform_pair(i, j, s_sta, 'N', wvf)
                  [tr_i_n, tr_j_n] = get_waveform_pair(i, j, s_sta, 'N', st_i, st_j)
                  if ((tr_i_n is not None) and (tr_j_n is not None)):
                     [difftt_sN, diffcc_sN, out_pair_fileN] = diff_pick_travel_time_xcorr(t0_origin_i, picks_i, tr_i_n, t0_origin_j, picks_j, tr_j_n, S_t_before, S_t_after, S_cc_maxlag, tr_i_n.stats.station, tr_i_n.stats.channel, 'S', str(out_id_i), str(out_id_j), out_plot_dtcc_dir, flag_plot)

                     if (diffcc_sN > diffcc_s): # choose differential travel time with highest CC on horiz components
                        difftt_s = difftt_sN
                        diffcc_s = diffcc_sN
                        out_pair_file = out_pair_fileN

                  count_S = add_diff_tt_cc_phase(difftt_s, diffcc_s, 'S', min_S_CC, max_S_CC, s_sta, flag_plot, out_pair_file, out_plot_dtcc_high_dir, diff_sta, diff_tt, diff_cc, diff_ph, count_S)

            # Output to dt.cc file
            if (diff_cc):
               fcc.write("# "+str(out_id_i)+" "+str(out_id_j)+" 0\n")
               count_pair += 1
               for kk in range(len(diff_cc)):
                  fcc.write(diff_sta[kk]+" "+('%8.6f' % diff_tt[kk])+" "+('%8.6f' % diff_cc[kk])+" "+diff_ph[kk]+"\n")

      t_total_i = time.time() - t_i
      print("Runtime (s), for event i: ", i, t_total_i)

   fcc.close()
   elapsed = time.time() - t
   print("Runtime (s) for dt.cc file:", elapsed)
   print("Number of pairs in dt.cc:", count_pair)
   print("Number of P phases in dt.cc:", count_P)
   print("Number of S phases in dt.cc:", count_S)


def main(IND_I_FIRST, IND_I_LAST, IND_J_FIRST, IND_J_LAST):
   print("##### PARTIALoutput_dtcc_readwaveform_file.py PROCESSING: IND_I_FIRST = ", IND_I_FIRST, ", IND_I_LAST = ", IND_I_LAST, ", IND_J_FIRST = ", IND_J_FIRST, ", IND_J_LAST = ", IND_J_LAST)

########################## Inputs and Outputs ########################## 
#   in_hinv_arc_file = '../LargeAreaEQTransformer/association/merged_magcat_locate_pr.arc' # use the merged file with magnitudes
   in_hinv_arc_file = '../LargeAreaEQTransformer/association/SMALLTEST_merged_magcat_locate_pr.arc' # use the merged file with magnitudes
   in_event_file_dir = '../LargeAreaEQTransformer/EventFiles/'

   str_dtcc_file = '_istart'+str(IND_I_FIRST).zfill(6)+'_iend'+str(IND_I_LAST).zfill(6)+'_jstart'+str(IND_J_FIRST).zfill(6)+'_jend'+str(IND_J_LAST).zfill(6)
#   out_dtcc_file = '../LargeAreaEQTransformer/HYPODD/LargeAreaEQTransformer_puerto_rico_dt_cc.txt'
   out_dtcc_file = '../LargeAreaEQTransformer/HYPODD/SMALLTEST_LargeAreaEQTransformer_puerto_rico_dt_cc'+str_dtcc_file+'.txt'
   out_plot_dtcc_dir = '../LargeAreaEQTransformer/Plots_dtcc/'
#   out_plot_dtcc_dir = '../LargeAreaEQTransformer/SMALLTEST_Plots_dtcc/'
   out_plot_dtcc_high_dir = '../LargeAreaEQTransformer/Plots_dtcc_high/'
   #out_plot_dtcc_high_dir = '../LargeAreaEQTransformer/SMALLTEST_Plots_dtcc_high/'
########################## Inputs and Outputs ########################## 

   if not os.path.exists(out_plot_dtcc_dir):
      os.makedirs(out_plot_dtcc_dir)
   if not os.path.exists(out_plot_dtcc_high_dir):
      os.makedirs(out_plot_dtcc_high_dir)

   output_dtcc(in_hinv_arc_file, in_event_file_dir, out_dtcc_file, out_plot_dtcc_dir, out_plot_dtcc_high_dir,
      IND_I_FIRST, IND_I_LAST, IND_J_FIRST, IND_J_LAST)


if __name__ == "__main__":

   if len(sys.argv) != 5:
      print("Usage: python PARTIALoutput_dtcc_readwaveform_file.py <i_start_ind> <i_end_ind> <j_start_ind> <j_end_ind>")
      sys.exit(1)

   IND_I_FIRST = int(sys.argv[1])
   IND_I_LAST = int(sys.argv[2])
   IND_J_FIRST = int(sys.argv[3])
   IND_J_LAST = int(sys.argv[4])

   main(IND_I_FIRST, IND_I_LAST, IND_J_FIRST, IND_J_LAST)

