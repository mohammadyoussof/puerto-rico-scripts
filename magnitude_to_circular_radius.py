import numpy as np

# Assume circular patch with 3 MPa stress drop
# Input magnitude -> get radius of rupture area

def get_circular_radius(mag):
   delta_sigma = 3.0e6
   moment = np.power(10.0, 1.5*(mag + 6.07)) 
   radius = np.power(((7/16.) * (moment / delta_sigma)), 1.0/3.0)
   return radius

#input_file = '../Catalog/catalog_new_puerto_rico_2018_2020.txt'
#output_file = '../Catalog/catalog_new_puerto_rico_2018_2020_diam.txt'

#input_file = '../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020.txt'
#output_file = '../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020_diam.txt'

#input_file = '../Catalog/EventsALL_FullEQTransformer_HypoSVI_Catalog_2018_2020_mag.txt'
#output_file = '../Catalog/EventsALL_FullEQTransformer_HypoSVI_Catalog_2018_2020_mag_diam.txt'

#input_file = '../Catalog/EQT_20200107_20200114/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114_maxdt2.txt'
#output_file = '../Catalog/EQT_20200107_20200114/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114_maxdt2_diam.txt'

#input_file = '../Catalog/EQT_20200107_20200114/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114_maxdt2_cc070.txt'
#output_file = '../Catalog/EQT_20200107_20200114/LargeAreaEQTransformer_GrowClust_puerto_rico_catalog_20200107_20200114_maxdt2_cc070_diam.txt'

#input_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr.txt'
#output_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_diam.txt'

#input_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOSVI_VELPRSN_Catalog.txt'
#output_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOSVI_VELPRSN_Catalog_diam.txt'

#input_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_GrowClust_VELPRSN_maxdt2.txt'
#output_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_GrowClust_VELPRSN_maxdt2_diam.txt'

#input_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'
#output_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_diam.txt'

#input_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOSVI_VELZHANG_Catalog.txt'
#output_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOSVI_VELZHANG_Catalog_diam.txt'

#input_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_GrowClust_VELZHANG_maxdt2.txt'
#output_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_GrowClust_VELZHANG_maxdt2_diam.txt'

#input_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_GrowClust_maxdt2_cc060.txt'
#output_file = '../Catalog/EQT_20200107_20200114/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_GrowClust_maxdt2_cc060_diam.txt'

#input_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'
#output_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_diam.txt'

#input_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOSVI_VELZHANG_Catalog.txt'
#output_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOSVI_VELZHANG_Catalog_diam.txt'

#input_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_GrowClust_VELZHANG_maxdt2.txt'
#output_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_GrowClust_VELZHANG_maxdt2_diam.txt'

#input_file = '../Catalog/EQT_20180101_20211001/catalog_new_puerto_rico_20180101_20211001_download20211020.txt'
#output_file = '../Catalog/EQT_20180101_20211001/catalog_new_puerto_rico_20180101_20211001_download20211020_diam.txt'

#input_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'
#output_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_diam.txt'

#input_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOSVI_VELZHANG_Catalog.txt'
#output_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOSVI_VELZHANG_Catalog_diam.txt'

#input_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_GrowClust_VELZHANG_maxdt2.txt'
#output_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_GrowClust_VELZHANG_maxdt2_diam.txt'

#input_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_NonCluster_GrowClust_VELZHANG_maxdt2.txt'
#output_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_NonCluster_GrowClust_VELZHANG_maxdt2_diam.txt'

#input_file = '../Catalog/EQT_20180101_20220101/catalog_new_puerto_rico_20180101_20220101_download20220111.txt'
#output_file = '../Catalog/EQT_20180101_20220101/catalog_new_puerto_rico_20180101_20220101_download20220111_diam.txt'

#input_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr.txt'
#output_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_diam.txt'

#input_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELPRSN_Catalog.txt'
#output_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELPRSN_Catalog_diam.txt'

#input_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_GrowClust_VELPRSN_maxdt2.txt'
#output_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_GrowClust_VELPRSN_maxdt2_diam.txt'

#input_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELPRSN_maxdt2.txt'
#output_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELPRSN_maxdt2_diam.txt'

#input_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'
#output_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_diam.txt'

#input_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_Catalog.txt'
#output_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_Catalog_diam.txt'

#input_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_maxdt2.txt'
#output_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_maxdt2_diam.txt'

input_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_maxdt2.txt'
output_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_maxdt2_diam.txt'



fout = open(output_file, 'w')
with open(input_file, 'r') as fin:
   for line in fin:
      split_line = line.strip('\n').split()
      input_mag = float(split_line[4])
      radius = get_circular_radius(input_mag)
      rad_km = radius/1000.
      print(split_line, radius, rad_km)
      fout.write(('%s %f\n') % (line.strip('\n'), rad_km))
fout.close()
