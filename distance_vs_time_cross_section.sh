#!/bin/bash

# Plot Puerto Rico earthquakes on a map

#gmt gmtset BASEMAP_TYPE plain
#gmt gmtset HEADER_FONT_SIZE 24p
#gmt gmtset LABEL_FONT_SIZE 16p # smaller font for labels
##gmt gmtset PLOT_DEGREE_FORMAT +DF
#gmt gmtset PLOT_DEGREE_FORMAT -DF
#gmt gmtset GRID_PEN_PRIMARY 0.1p
#
gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

out_color_filename=depthcolors.cpt
out_color_file=../Plots/${out_color_filename}

# HYPOINVERSE
in_file_strike1=../Plots/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020_depth_0_1035_seismicity_time_projection15.txt
out_map_strike1=../Plots/DT_FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020_depth_0_1035_projection15
in_file_strike2=../Plots/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020_depth_0_1035_seismicity_time_projection95.txt
out_map_strike2=../Plots/DT_FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_catalog_combined_2018_2020_depth_0_1035_projection95

# HYPOSVI
#in_file_strike1=../Plots/FullEQTransformer_REAL_HYPOSVI_puerto_rico_catalog_combined_2018_2020_depth_0_1035_seismicity_time_projection15.txt
#out_map_strike1=../Plots/DT_FullEQTransformer_REAL_HYPOSVI_puerto_rico_catalog_combined_2018_2020_depth_0_1035_projection15
#in_file_strike2=../Plots/FullEQTransformer_REAL_HYPOSVI_puerto_rico_catalog_combined_2018_2020_depth_0_1035_seismicity_time_projection95.txt
#out_map_strike2=../Plots/DT_FullEQTransformer_REAL_HYPOSVI_puerto_rico_catalog_combined_2018_2020_depth_0_1035_projection95


# Time slice ranges
FIRST_DAY=726 # 2019-12-28
#LAST_DAY=761
LAST_DAY=1035 # 2020-11-01


# Cross section parameters
#strike1_angle=15
#strike1_length=30
#strike1_proj_width=10
#strike1_center_lat=17.9
#strike1_center_lon=-66.85
#strike1_bproj=-JX30/15
#strike1_brange=-R-30/30/-30/0

strike1_bproj=-JX30/20
strike1_brange=-R${FIRST_DAY}/${LAST_DAY}/-30/30



#strike2_angle=95
#strike2_length=40
#strike2_proj_width=10
#strike2_center_lat=17.93
#strike2_center_lon=-66.85
#strike2_bproj=-JX40/15
#strike2_brange=-R-40/40/-30/0

strike2_bproj=-JX30/30
strike2_brange=-R${FIRST_DAY}/${LAST_DAY}/-40/40


# Entire Model1 time range
#FIRST_DAY=0 # 2018-01-01
#LAST_DAY=1035 # 2020-11-01

# Before start of sequence
#FIRST_DAY=0 # 2018-01-01
#LAST_DAY=726 # 2019-12-28

# Between start of sequence and mainshock
#FIRST_DAY=726 # 2019-12-28
#LAST_DAY=736 # 2020-01-07

# First week after mainshock
#FIRST_DAY=736 # 2020-01-07
#LAST_DAY=743 # 2020-01-14

# First day after mainshock
#FIRST_DAY=736 # 2020-01-07
#LAST_DAY=737 # 2020-01-08

# Day 1-4 after mainshock
#FIRST_DAY=737 # 2020-01-08
#LAST_DAY=740 # 2020-01-11

# Day 5-7 after mainshock
#FIRST_DAY=740 # 2020-01-11
#LAST_DAY=743 # 2020-01-14

# Day 7-14 after mainshock
#FIRST_DAY=743 # 2020-01-14
#LAST_DAY=750 # 2020-01-21

# Day 14-25 after mainshock - to end of 2020-01
#FIRST_DAY=750 # 2020-01-21
#LAST_DAY=761 # 2020-02-01

# 2020-02
#FIRST_DAY=761 # 2020-02-01
#LAST_DAY=790 # 2020-03-01

# 2020-03
#FIRST_DAY=790 # 2020-03-01
#LAST_DAY=821 # 2020-04-01

# 2020-04
#FIRST_DAY=821 # 2020-04-01
#LAST_DAY=851 # 2020-05-01

# 2020-05
#FIRST_DAY=851 # 2020-05-01
#LAST_DAY=882 # 2020-06-01

# 2020-06
#FIRST_DAY=882 # 2020-06-01
#LAST_DAY=912 # 2020-07-01

# 2020-07
#FIRST_DAY=912 # 2020-07-01
#LAST_DAY=943 # 2020-08-01

# 2020-08
#FIRST_DAY=943 # 2020-08-01
#LAST_DAY=974 # 2020-09-01

# 2020-09
#FIRST_DAY=974 # 2020-09-01
#LAST_DAY=1004 # 2020-10-01

# 2020-10
#FIRST_DAY=1004 # 2020-10-01
#LAST_DAY=1035 # 2020-11-01



##### COLOR BY DEPTH #####

gmt makecpt -Cinferno -I -T0/20/0.01 > ${out_color_file}

gmt begin ${out_map_strike1}_${FIRST_DAY}_${LAST_DAY}
   gmt basemap ${strike1_bproj} ${strike1_brange} -Ba15f -BneWS #-U
   sort -nk6,6 ${in_file_strike1} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $8, $3, $5*$5*0.01 + 0.0200}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Event time along cross-section A-A'" -Bya2+l"Distance along cross-section A-A' (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_file} # HYPOINVERSE
#   sort -nk6,6 ${in_file_strike1} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $11, $3, $5*$5*0.01 + 0.0200}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa2+l"Event time along cross-section A-A'" -Bya2+l"Distance along cross-section A-A' (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_file} # HYPOSVI 
   gmt colorbar -C${out_color_file} -Dx11.9i/2.0i/2.5i/0.15i -Bxaf+l"Depth (km)"
gmt end show

gmt begin ${out_map_strike2}_${FIRST_DAY}_${LAST_DAY}
   gmt basemap ${strike2_bproj} ${strike2_brange} -Ba15f -BneWS #-U
   sort -nk6,6 ${in_file_strike2} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $8, $3, $5*$5*0.01 + 0.0200}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa2+l"Event time along cross-section B-B'" -Bya2+l"Distance along cross-section B-B' (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_file} # HYPOINVERSE
#   sort -nk6,6 ${in_file_strike2} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $6/86400., $11, $3, $5*$5*0.01 + 0.0200}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa2+l"Event time along cross-section B-B'" -Bya2+l"Distance along cross-section B-B' (km)" -BneWS -Sc -W0.01c -G255 -C${out_color_file} # HYPOSVI 
   gmt colorbar -C${out_color_file} -Dx11.9i/2.0i/2.5i/0.15i -Bxaf+l"Depth (km)"
gmt end show


