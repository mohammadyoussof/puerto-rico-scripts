import json
import time
import numpy as np
import matplotlib.pyplot as plt
import utils_hypoinverse as utils_hyp
import utils_magnitude as utils_mag

# Compute local magnitude (Wood-Anderson) for EQTransformer events, following magnitude calibration from matching catalog events


#in_hinv_arc_file = '../LargeAreaEQTransformer/association/merged_locate_pr.arc' # use the merged file
#in_match_catalog_file = '../LargeAreaEQTransformer/association/events_MATCH_eqt_20200107_20200114.txt'
##in_match_catalog_file = '../LargeAreaEQTransformer/association/TEST_events_MATCH_eqt_20200107_20200114.txt'
#in_station_file = '../LargeAreaEQTransformer/station_list_edited.json'
##in_calib_file = '../LargeAreaEQTransformer/association/magnitude_calibration_20200107_20200114.txt'
#in_calib_file = '../LargeAreaEQTransformer/association/constrainwindow_magnitude_calibration_20200107_20200114.txt'
##in_calib_file = '../FullEQTransformer/association/magnitude_calibration_2018_2020.txt'
##in_event_file_dir = '../LargeAreaEQTransformer/EventFiles/'
#in_event_file_dir = '../LargeAreaEQTransformer/EventFiles120/'
#in_inv_dir = '../LargeAreaEQTransformer/downloads_mseeds/StationXML/'

in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_locate_pr.arc' # use the merged file
in_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/events_MATCH_eqt_2018_2020.txt'
in_station_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
in_calib_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/constrainwindow120_magnitude_calibration_2018_2020.txt'
#in_event_file_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/EventFiles/'
in_event_file_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/EventFiles120/'
in_inv_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/StationXML/'

#out_mag_plot_dir = '../LargeAreaEQTransformer/MagPlots/'
##out_calib_plot_file = 'calib_NOFILTERPHASETEST_20200107_20200114_catalog_match_event_mag_comparison.png'
##out_calib_plot_file = 'median_calib_NOFILTERPHASETEST_20200107_20200114_catalog_match_event_mag_comparison.png'
#out_calib_plot_file = 'constrainwindow120_median_calib_NOFILTERPHASETEST_20200107_20200114_catalog_match_event_mag_comparison.png'
##out_calib_plot_file = 'FULL_median_calib_NOFILTERPHASETEST_20200107_20200114_catalog_match_event_mag_comparison.png'
#out_match_catalog_file = '../LargeAreaEQTransformer/association/events_MATCH_magcat_eqt_20200107_20200114.txt'
#out_hinv_arc_file = '../LargeAreaEQTransformer/association/merged_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
#out_hinv_sum_file = '../LargeAreaEQTransformer/association/merged_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

out_mag_plot_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/MagPlots/'
out_calib_plot_file = 'constrainwindow120_FULL_median_calib_NOFILTERPHASETEST_2018_2020_catalog_match_event_mag_comparison.png'
out_match_catalog_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/events_MATCH_magcat_eqt_2018_2020.txt'
out_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_magcat_locate_pr.arc' # catalog magnitudes filled into HYPOINVERSE arc file
out_hinv_sum_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/merged_magcat_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file

min_lat = 17.6
max_lat = 18.3
min_lon = -67.3
max_lon = -66.4

# Parameters for event time window to look for peak amplitude
start_time = 15 # start time for window: seconds from beginning of event file (at origin time)
#end_time = 55 # end time for window: seconds from beginning of event file
end_time = 115 # end time for window: seconds from beginning of event file
min_time = 5 # minimum window length in seconds
flag_filter = False # flag for bandpass filter
min_end_time = start_time + min_time

# Get station data
stations_ = json.load(open(in_station_file))

# Get station inventory for instrument response
inv_map = utils_mag.get_station_inventory(in_inv_dir, stations_)

# Get all event data from HYPOINVERSE arc file
map_events = utils_mag.get_events_map(in_hinv_arc_file, stations_)
map_cat_events = utils_mag.get_catalog_events_map(in_match_catalog_file, stations_)

# Get all event phase data from HYPOINVERSE arc file
[event_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)

# Distance correction, slope and intercept - get this from matching catalog events
# Run calibrate_local_magnitude_wood_anderson.py
#   dist_corr_slope =  0.725917698378 # TEST 20200107-20200114
#   dist_corr_icpt =  3.76802638352 # TEST 20200107-20200114
#   dist_corr_slope = 1.38657435857 # PHASETEST 20200107-20200114
#   dist_corr_icpt = 2.58957042452 # PHASETEST 20200107-20200114
#dist_corr_slope = 1.41556597881 # NOFILTERPHASETEST 20200107-20200114
#dist_corr_icpt = 2.49115637769 # NOFILTERPHASETEST 20200107-20200114
#dist_corr_slope = 1.18039588465 # NOFILTERPHASETEST 20200107-20200114 StationXML
#dist_corr_icpt = 3.00651850763 # NOFILTERPHASETEST 20200107-20200114 StationXML

# Distance correction, slope (common) and intercept (per station) - get this from matching catalog events
order_sta = []
dist_corr_icpt = []
with open(in_calib_file, 'r') as fin:
   for line in fin:
      if (line[0] == 'k'):
         dist_corr_slope = float(line.split()[1])
      else:
         split_line = line.split()
         order_sta.append(split_line[0])
         dist_corr_icpt.append(float(split_line[1]))

# Loop over all events
t0 = time.time()
for ev_id in map_events:

#for ev_id in map_cat_events:
#   # Skip if event is located out of bounds (don't want to include faraway regional events) - CATALOG COMPARISON ONLY
#   ev_lat = event_dict[ev_id]['event'][1]
#   ev_lon = event_dict[ev_id]['event'][2]
#   if ((ev_lat < min_lat) or (ev_lat > max_lat) or (ev_lon < min_lon) or (ev_lon > max_lon)):
#      continue

   median_ML_Richter = utils_mag.calculate_local_magnitude(in_event_file_dir, event_dict, ev_id, order_sta, map_events,
      inv_map, flag_filter, start_time, end_time, min_time, min_end_time, dist_corr_slope, dist_corr_icpt)
   map_events[ev_id].append(median_ML_Richter) # ML is the last item in the map values
print("Total runtime magnitude calculation: ", time.time()-t0)

# After magnitude calibration, show that magnitudes match for catalog events only: save arrays for plots
all_catalog_mag = []
all_catalog_ML = []
for ev_id in map_cat_events: # Loop over catalog events only
   if (len(map_events[ev_id]) > 6): # ML exists for this event, last item in map for all events
      all_catalog_mag.append(map_cat_events[ev_id][3]) # Get magnitude from catalog
      all_catalog_ML.append(map_events[ev_id][6]) # Get ML from map for all events

# After magnitude calibration, show that magnitudes match: plot catalog magnitude vs calculated ML
equal_line = np.linspace(-1, 7, 100)
plt.figure(figsize=(6,8))
plt.scatter(all_catalog_mag, np.asarray(all_catalog_ML), facecolors='none', edgecolors='k')
plt.plot(equal_line, equal_line, '--', color='k')
plt.axis('scaled')
plt.xlim([0, 7])
plt.ylim([0, 7])
plt.xticks([0, 1, 2, 3, 4, 5, 6, 7])
plt.yticks([0, 1, 2, 3, 4, 5, 6, 7])
plt.xlabel('Catalog magnitude $M_{cat}$')
plt.ylabel('Local magnitude $M_{L}$')
plt.savefig(out_mag_plot_dir+out_calib_plot_file)

# Output magnitudes to file: matching catalog events
fout_match = open(out_match_catalog_file, 'w')
with open(in_match_catalog_file, 'r') as fin:
   for line in fin:
      ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line))
      if (ev_id in map_cat_events):
         ev_mag = 0.0
         if (len(map_cat_events[ev_id]) > 6): # ML exists for this event, last item in map for all events
            ev_mag = int(round(100*map_events[ev_id][6]))
         fout_match.write(('%s%3d%s') % (line[0:147], ev_mag, line[150:]))
fout_match.close()

# Output magnitudes to HYPOINVERSE files: all events
fout_arc = open(out_hinv_arc_file, 'w')
fout_sum = open(out_hinv_sum_file, 'w')
with open(in_hinv_arc_file, 'r') as fin:
   for line in fin:
      if ((line[0:2] == '19') or (line[0:2] == '20')): # Replace magnitude in line
         ev_id = int(utils_hyp.get_event_id_hypoinverse_file(line))
         if (ev_id in map_events):
            ev_mag = 0.0
            if (len(map_events[ev_id]) > 6): # ML exists for this event, last item in map for all events
               ev_mag = int(round(100*map_events[ev_id][6]))
            fout_arc.write(('%s%3d%s') % (line[0:147], ev_mag, line[150:]))
            fout_sum.write(('%s%3d%s') % (line[0:147], ev_mag, line[150:]))
      else:
         if (ev_id in map_events):
            fout_arc.write(('%s') % line)
fout_arc.close()
fout_sum.close()

