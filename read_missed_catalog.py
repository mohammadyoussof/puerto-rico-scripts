import datetime

# Read in missed catalog events and output in plot-friendly format

#in_missed_event_file = '../LargeAreaEQTransformer/association/events_MISSED_magcat_eqt_20200107_20200114.txt'
#catalog_start_time='2020-01-07T00:00:00'
#out_missed_event_file = '../Catalog/LargeAreaEQTransformer_HYPOINVERSE_puerto_rico_MISSED_20200107_20200114.txt'

#in_missed_event_file = '../LargeAreaEQTransformer/REAL/Events/events_MISSED_magcat_eqt_20200107_20200114.txt'
#catalog_start_time='2020-01-07T00:00:00'
#out_missed_event_file = '../Catalog/LargeAreaEQTransformerModel2_REAL_HYPOINVERSE_puerto_rico_MISSED_20200107_20200114.txt'
##out_missed_event_file = '../Catalog/LargeAreaEQTransformer_REAL_HYPOINVERSE_puerto_rico_MISSED_20200107_20200114.txt'

#in_missed_event_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/association/events_MISSED_magcat_eqt_2018_2020.txt'
#catalog_start_time = '2018-01-01T00:00:00'
#out_missed_event_file = '../Catalog/FullEQTransformer_HYPOINVERSE_puerto_rico_MISSED_2018_2020.txt'

#in_missed_event_file = '../FullEQTransformer/REAL/HYPOINVERSE/events_MISSED_magcat_eqt_2018_2020.txt'
#catalog_start_time = '2018-01-01T00:00:00'
#out_missed_event_file = '../Catalog/FullEQTransformer_REAL_HYPOINVERSE_puerto_rico_MISSED_2018_2020.txt'

#-----------------------------------
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/events_MISSED_magcat_EQT_20180101_20210601.txt'
#catalog_start_time = '2018-01-01T00:00:00'
#out_missed_event_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOINVERSE_VELPRSN_events_MISSED_magcat.txt'

#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_MISSED_magcat_EQT_20180101_20210601.txt'
#catalog_start_time = '2018-01-01T00:00:00'
#out_missed_event_file = '../Catalog/EQT_20180101_20210601/EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_events_MISSED_magcat.txt'

#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/events_MISSED_magcat_EQT_20180101_20211001.txt'
#catalog_start_time = '2018-01-01T00:00:00'
#out_missed_event_file = '../Catalog/EQT_20180101_20211001/EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_events_MISSED_magcat.txt'

#-----------------------------------
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MISSED_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = '2018-01-01T00:00:00'
#out_missed_event_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_events_MISSED_magcat.txt'

in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MISSED_magcat_EQT_20180101_20220101.txt'
catalog_start_time = '2018-01-01T00:00:00'
out_missed_event_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_events_MISSED_magcat.txt'


catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
fout = open(out_missed_event_file, 'w')
with open(in_missed_event_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      origin_time = datetime.datetime.strptime(split_line[0], '%Y-%m-%dT%H:%M:%S.%f')
      num_sec = (origin_time - catalog_ref_time).total_seconds()
      lat_deg = float(split_line[1])
      lon_deg  = float(split_line[2])
      depth_km = float(split_line[3])
      mag = float(split_line[4])
      evid = split_line[5].strip('.')
      fout.write(("%f %f %f %f %f %s\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
fout.close()
