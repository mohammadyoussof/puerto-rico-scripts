import json
import os
from obspy import UTCDateTime
from obspy.clients.fdsn import Client


def main():
#   in_sta_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/station_list_edited.json'
#   out_base_dir = '/media/yoon/INT01/PuertoRico/FullEQTransformer/StationXML/'
#   start_time = UTCDateTime("2018-01-01T00:00:00")
#   end_time = UTCDateTime("2021-02-01T00:00:00")
##   end_time = UTCDateTime("2020-11-01T00:00:00")

#   in_sta_file = '../LargeAreaEQTransformer/station_list_edited.json'
#   out_base_dir = '../LargeAreaEQTransformer/StationXML/'
#   start_time = UTCDateTime("2020-01-07T00:00:00")
#   end_time = UTCDateTime("2020-01-14T00:00:00")

#   in_sta_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/station_list_edited.json'
#   out_base_dir = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/StationXML/'
#   start_time = UTCDateTime("2020-01-07T00:00:00")
#   end_time = UTCDateTime("2020-01-14T00:00:00")

#   in_sta_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/station_list_edited.json'
#   out_base_dir = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/StationXML/'
#   start_time = UTCDateTime("2019-12-28T00:00:00")
#   end_time = UTCDateTime("2020-01-14T00:00:00")

   in_sta_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/station_list_edited.json'
   out_base_dir = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/StationXML/'
   start_time = UTCDateTime("2018-01-01T00:00:00")
#   end_time = UTCDateTime("2021-06-01T00:00:00")
#   end_time = UTCDateTime("2021-08-01T00:00:00")
#   end_time = UTCDateTime("2021-09-01T00:00:00")
#   end_time = UTCDateTime("2021-10-01T00:00:00")
   end_time = UTCDateTime("2022-01-01T00:00:00")

   if not os.path.exists(out_base_dir):
      os.makedirs(out_base_dir)

   client = Client("IRIS")
   json_file = open(in_sta_file)
   stations_ = json.load(json_file)

   for sta,val in stations_.items():
      net = val['network']
      out_stationxml_dir = out_base_dir+sta+'/'
      if not os.path.exists(out_stationxml_dir):
         os.makedirs(out_stationxml_dir)
      out_stationxml_file = out_stationxml_dir+net+'.'+sta+'.xml'
      inv = client.get_stations(network=net, station=sta, starttime=start_time, endtime=end_time, level='response', filename=out_stationxml_file, format='xml')


if __name__ == "__main__":
   main()
