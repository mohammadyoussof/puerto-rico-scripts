# dtcc file input: only station code (max 5 characters for GrowClust input)
# dtcc file output: network+station code (for HYPODD input from HYPOINVERSE)

in_dtcc_file = '../LargeAreaEQTransformer/HYPODD/combined_LargeAreaEQTransformer_puerto_rico_dt_cc.txt'
out_dtcc_file = '../LargeAreaEQTransformer/HYPODD/out_LargeAreaEQTransformer_puerto_rico_dt_cc.txt'

fout = open(out_dtcc_file,'w')
with open(in_dtcc_file,'r') as fin:
   for line in fin:
      if (line[0] == '#'):
         fout.write(line)
#         print(line)
      else:
         split_line = line.split()
         sta_name = split_line[0]
         if ((sta_name == 'PR01') or (sta_name == 'PR02') or (sta_name == 'PR03') or (sta_name == 'PR04') or (sta_name == 'PR05') or (sta_name == 'PR06')):
            netsta_name = 'GS'+sta_name
         elif (sta_name == 'SJG'):
            netsta_name = 'IU'+sta_name
         else:
            netsta_name = 'PR'+sta_name
#         print(netsta_name+" "+('%8.6f' % float(split_line[1]))+" "+('%8.6f' % float(split_line[2]))+" "+split_line[3]+"\n")
         fout.write(netsta_name+" "+('%8.6f' % float(split_line[1]))+" "+('%8.6f' % float(split_line[2]))+" "+split_line[3]+"\n")

fout.close()
